# lerna-typescript-cra-uilib-starter
Starter for Monorepos: Lerna, TypeScript, CRA and Storybook

How to run:

1 yarn
2 npm run prestart //to build you project
3 npm run start //it will start project on localhost

How to add new dependency:

# Adds the module-1 package to the packages in the 'prefix-' prefixed folders
lerna add module-1 packages/prefix-*

# Install module-1 to module-2
lerna add module-1 --scope=module-2

# Install module-1 to module-2 in devDependencies //module-1 = dependency, module-2 = package name
lerna add module-1 --scope=module-2 --dev //lerna add babel-core --scope=@my-org/my-monorepo-ui-lib --dev

# Install module-1 in all modules except module-1
lerna add module-1 //lerna add babel-core

# Install babel-core in all modules
lerna add babel-core

# Added post build scripts for CI/CD process