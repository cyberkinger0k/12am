import { storiesOf } from "@storybook/react";
import React from "react";
import { Button, DataGrid } from "../components";
import Checkbox from '@material-ui/core/Checkbox';

const columnDefs = [{
    headerName: "Make", field: "make", sortable: true, filter: true,
}, {
    headerName: "Model", field: "model", sortable: true, filter: true,
}, {
    headerName: "Price", field: "price", sortable: true, filter: true,
}];
const rowData = [
    {
    make: "Toyota", model: "Celica", price: 35000,
}, {
    make: "Ford", model: "Mondeo", price: 32000,
}, {
    make: "Porsche", model: "Boxter", price: 72000,
}
];

const column = [{
    headerName: "Session", field: "session", sortable: true, filter: true, width: 150,lockPosition: true,
}, {
    headerName: "Title", field: "title", sortable: false, filter: false, width: 350,
}, {
    headerName: "Presented By", field: "Presented", sortable: false, filter: false, width: 180,
}, {
    headerName: "Date", field: "date", sortable: false, filter: false, width: 150,
}, {
    headerName: "submission", field: "submission", sortable: true, filter: false, width: 150,
    cellRendererFramework: () => {
        return <a onClick={this.cellClickedHandle}>60%</a>;
    },
}, {
    headerName: "Edit", field: "Edit", sortable: true, filter: false, width: 80,
    cellRendererFramework:  () => {
           return(
           <Button className="inbox-edit-button" variant="contained" btnColor="primary"
                    onClick={this.buttonClickedHandle}>Edit
            </Button>
                    );
    },
}, {
    headerName: "Enable", field: "", sortable: true, filter: false, width: 70,
    cellRendererFramework:  () => {
           return <Checkbox className="inbox-checkbox"  onClick={this.checkboxClickedHandle}/>;
    },
},

];

const row = [
//    session : "session #25" , title : "planning and Releasing software with JIRA", Presented: "Anil Pandey", date: "14-sep-19", status: "Submitted",
// },
// {
//     session : "session #24" , title : "Agile Foundation", Presented: "Rakesh Saytode", date: "14-sep-19", status: "Submitted",
//  },
//  {
//     session : "session #23" , title : "Git Essential Training", Presented: "Gaurav Agrawal", date: "30-Aug-19", status: "Submitted",
//  },
//  {
//     session : "session #22" , title : "Behaviour Driven Development", Presented: "Ravi Sahu", date: "24-Aug-19", status: "Submitted",
//  },
//  {
//     session : "session #21" , title : "Linux Commands", Presented: "Ajay Thakur", date: "16-Aug-19", status: "Submitted",
//  },
//  {
//     session : "session #20" , title : "Web Programming Foundations", Presented: "Shailendra Gautam", date: "10-Aug-19", status: "Submitted",
//  },
//  {
//     session : "session #19" , title : "Cloud Computing Core Concepts", Presented: "Ravikant Tiwari", date: "02-Aug-19", status: "Submitted",
//  },
];

const loadingTemplate = '<span class="ag-overlay-loading-center">Please wait while your rows are loading</span>';
const noRowsTemplate = "<span style=\"padding: 10px; border: 2px solid #444; background: lightgoldenrodyellow;\">This is a custom 'no rows' overlay</span>";

storiesOf("DataGrid", module)
    .add("DataGrid",
        () =>
            <div>
                <h4>Ag grid showcase </h4>
                <DataGrid className="data-grid-1"
                    columns={columnDefs}
                    rows={rowData} 
                    rowDrag={false}
                    /><br/>
                 <DataGrid className="data-grid-2"
                    columns={column}
                    rows={row}
                    loadingTemplate={loadingTemplate}
                    noRowsTemplate={noRowsTemplate} />
            </div>
    );
