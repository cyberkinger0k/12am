import { withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Button, Cards } from "../components";

storiesOf("Card", module)
    .addDecorator(withKnobs)
    .add("Card",
        () =>
      <Cards id="card" className="card">

        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
           className="card-img-top" />
        <div className="card-body">
        <h4 className="card-title">Lizard</h4>
        <p className="card-text"> Lizards are a widespread group of squamate reptiles, with over 6,000 species,
          ranging across all continents except Antarctica</p>
        <Button size="small" btnColor="primary">
          Share
        </Button>
        <Button size="small" btnColor="primary">
          Learn More
        </Button>
        </div>

 </Cards>
    );
