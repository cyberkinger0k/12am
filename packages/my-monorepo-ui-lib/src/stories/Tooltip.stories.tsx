import { storiesOf } from "@storybook/react";
import React from "react";
import { MToolTip , Button} from "../components";

storiesOf("Tooltip", module)
.add("Tooltip", () =>
  <div>
      <h2>Simple Tooltip</h2>
      <MToolTip title="tooltip" id="tooltip-1" placement={"left"}>
        <span><Button variant="contained" btnColor="primary">Button</Button></span>
      </MToolTip>
     
     <h2>Disabled Tooltip</h2>
      <MToolTip title="You don't have permission to do this">
       <span>
         <Button disabled>Disabled</Button>
       </span>
      </MToolTip>
  </div>

);
