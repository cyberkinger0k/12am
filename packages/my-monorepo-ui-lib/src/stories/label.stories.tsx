import { storiesOf } from "@storybook/react";
import React from "react";
import { Label } from "../components";

storiesOf("Label", module)
    .add("Label",
        () =>  <Label content="STORY"/>,
    );
