import { storiesOf } from "@storybook/react";
import React from "react";
import { Li, ListItems, ListButton, Button, DropDown } from "../components";
import { Grid } from "@material-ui/core";

const list = [
    {   id: 1,
        value: "Option 1",
    },
    {
        id: 2,
        value: "Option 2",
    },
    {
        id: 3,
        value: "Option 3",
    },
    ,
    {
        id: 4,
        value: "Option 4",
    }
];
storiesOf("List", module)
    .add("List",
        () =>
        <div>
        <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
            <h4>Simple list with content</h4>
            <Li>
                <ListItems
                primaryContent="hello"
                secondaryContent="its list component"
                />
                <ListItems
                primaryContent="hello"
                secondaryContent="its list component"
                />
                <ListItems
                primaryContent="hello"
                secondaryContent="its list component"
                />
            </Li>
        </Grid>

        <Grid item xs={12} md={6}>
        <h4>List with Avatar</h4>
        <Li>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            />
            <ListItems
            primaryContent="hello"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >   
            </ListItems>
            <ListItems
            primaryContent="hello"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            />
        </Li>
        </Grid>

        <Grid item xs={12} md={6}>
        <h4>List with Letter Avatar and dropdown</h4>
        <Li>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatardata="A"
            >
                <ListButton>
                <DropDown className="dropdown" id="dropdown" value={list}  label="DropDown" />
                </ListButton>    
            </ListItems>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatardata="B"
            >
                <ListButton>
                <DropDown className="dropdown" id="dropdown" value={list}  label="DropDown" />
                </ListButton>    
            </ListItems>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatardata="shruti"
            >
                <ListButton>
                <DropDown className="dropdown" id="dropdown" value={list}  label="DropDown" />
                </ListButton>    
            </ListItems>
        </Li>
        </Grid>

        <Grid item xs={12} md={6}>
        <h4>List with Button</h4>
        <Li>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >
                <ListButton>
                <Button variant="contained" btnColor="primary">add</Button>
                <Button variant="contained" btnColor="secondary">del</Button>
                <Button variant="contained" btnColor="default">info</Button>
                </ListButton>    
            </ListItems>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >
                <ListButton>
                <Button variant="contained" btnColor="primary">add</Button>
                <Button variant="contained" btnColor="secondary">del</Button>
                <Button variant="contained" btnColor="default">info</Button>
                </ListButton>    
            </ListItems>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >
                <ListButton>
                <Button variant="contained" btnColor="primary">add</Button>
                <Button variant="contained" btnColor="secondary">del</Button>
                <Button variant="contained" btnColor="default">info</Button>
                </ListButton>    
            </ListItems>
        </Li>
        </Grid>
        </Grid>
        </div>
    );