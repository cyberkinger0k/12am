import { storiesOf } from "@storybook/react";
import React from "react";
import { FacebookLogin, GoogleLogin } from "../components/socialLogin";

storiesOf("Social Login", module)
    .add("Facebook Login",
        () =>
        <div>
            <h3>Facebook Login Component</h3>
            <FacebookLogin appId={"749437365553344"}/>
        </div>
    )
    .add("Google Login",
        () =>
        <div>
            <h3>Google Login Component</h3>
            <GoogleLogin clientId={"749437365553344"}
            scope={""}/>
        </div>
    );