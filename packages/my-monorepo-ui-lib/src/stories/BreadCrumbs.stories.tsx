// import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Breadcrumbs } from "../components";
import { Link, BrowserRouter } from "react-router-dom";

storiesOf("BreadCrumbs", module)
.add("BreadCrumbs", () =>
  <div>
  <h3>Simple BreadCrumbs</h3>
  <Breadcrumbs className="breadcrumbs-1" data={["devesh","sahu"]} onClick={(path) => <BrowserRouter><Link to={path}/></BrowserRouter> }/>

  <h3>Custom BreadCrumbs</h3>
  {/* <Breadcrumbs separator=">" className="breadcrumbs-2">
        <Link color="inherit" href="/" >Material-UI </Link>
        <Link color="inherit" href="/" >Core</Link>
        <Typography color="textPrimary">Breadcrumb</Typography>
  </Breadcrumbs> <br/>
  <Breadcrumbs separator="-" className="breadcrumbs-3">
        <Link color="inherit" href="/" >Material-UI </Link>
        <Link color="inherit" href="/" >Core</Link>
        <Typography color="textPrimary">Breadcrumb</Typography>
  </Breadcrumbs> */}

  </div>,
);

