import React from "react";
import { storiesOf } from '@storybook/react';
import { Chip } from "../components/chipInput";

let yourChips;
const dataSource = ["devesh", "sahu"];
const chipRenderer = () => {
    return dataSource;
}
storiesOf("Chip", module)
.add("Chip", () =>
    <Chip
    name="chip"
    handleAddChips={(chip) => {
        yourChips.push(chip);
    }}
    handleDeleteChips={(chip: any, index: number) => {
        yourChips.splice(index, 1);
    }}
    dataSource={dataSource}
    chipRenderer={chipRenderer}
    />
);