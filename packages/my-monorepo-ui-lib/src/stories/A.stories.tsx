import { storiesOf } from "@storybook/react";
import React from "react";
import { A } from "../components";

storiesOf("Anchor", module)
    .add("Anchor",
        () => <A content="Hello world" />,
    );
