import { withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Span, TypographyComponent } from "../components";

storiesOf("TypographyComponent", module)
    .addDecorator(withKnobs)
    .add("headings",
        () =>
    <div>
    <TypographyComponent variant={"h1"}  >
        {"h1. Heading"}
      </TypographyComponent>
      <TypographyComponent variant={"h2"} >
       {" h2. Heading"}
      </TypographyComponent>
      <TypographyComponent variant={"h3"} >
        {"h3. Heading"}
      </TypographyComponent>
      <TypographyComponent variant={"h4"} >
        {"h4. Heading"}
      </TypographyComponent>
      <TypographyComponent variant={"h5"} >
       {"h5. Heading"}
      </TypographyComponent>
      <TypographyComponent variant="h6" >
        {"h6. Heading"}
      </TypographyComponent>    </div>,
    )
    .add("Span",
     () =>
     <div>
     <Span>Stories</Span>
     <br />
     <Span>Stories</Span>
     </div>,
    );
