import { storiesOf } from "@storybook/react";
import React from "react";
import { CommentTable, CommentTableRow } from "../components";

storiesOf("Comment Table", module)
    .add("Comment Table",
        () =>  <CommentTable>
                    <CommentTableRow
                    name="shruti bajaj"
                    comment="The .table class adds basic styling (light padding and only horizontal dividers) to a table"
                    datetime="01/02/2020" />
                    <CommentTableRow
                    name="shruti"
                    comment="The .table class adds basic styling (light padding and only horizontal dividers) to a table"
                    datetime="01/02/2020"
                    commentType="worklog"/>
                </CommentTable>
                ,
    );
