// import { action } from "@storybook/addon-actions";
import { withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import {Sidebar} from "../components";
import { MdDashboard, MdFeedback, MdInbox } from 'react-icons/md';
// import { withTests } from '@storybook/addon-jest';

const nav = [
  { path: "/dashboard", label: "Dashboard", icon: <MdDashboard style={{height: '24px', width:'24px'}}/> },
  { path: "/ticket", label: "Workflow", icon: <MdInbox style={{height: '24px', width:'24px'}}/> },
  { path: "/feedback", label: "Feedback", icon: <MdFeedback style={{height: '24px', width:'24px'}}/>,
   children: [{ path: "/feedback", label: "Feedback"}]},
  ]

storiesOf("Sidebar", module)
    .addDecorator(withKnobs)
    .add("basic sidebar",
        () => (
            <div>
                <Sidebar routes={nav}/>
            </div>
        ),

    );
