import { storiesOf } from "@storybook/react";
import React from "react";
import { CheckBox } from "../components";
import "../components/checkbox/CheckBox.scss";

storiesOf("CheckBox", module)
.add("CheckBox", () =>
  <div>
      <h1>Checkbox</h1>
      <h3>Default-color</h3>
      <CheckBox className="check-box" color="default" />
      <CheckBox className="check-box" color="default" checked/>
      <h3>Primary-color</h3>
      <CheckBox className="check-box" color="primary"/>
      <CheckBox className="check-box" color="primary" checked/>
      <h3>Secondary-color</h3>
      <CheckBox className="check-box" color="secondary"/>
      <CheckBox className="check-box" color="secondary" checked/>
      <h3>Custom-color</h3>
      <CheckBox className="green-check-box" color="default"/>
      <CheckBox className="green-check-box" color="default" checked/>
      <h3>Disabled</h3>
      <CheckBox className="check-box" color="secondary" checked disabled/>
  </div>
);
