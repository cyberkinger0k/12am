import { text, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { ProgressBar } from "../components";
import { Grid } from "@material-ui/core";

storiesOf("Progress Bar", module)
    .addDecorator(withKnobs)
    .add("progress bar",
        () => 
        <div>
        <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
                <h4>Progress Bar</h4>
                <ProgressBar progressclassName={"bg-info"} width={"25%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-success"} width={"50%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-warning"} width={"75%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-danger"} width={"100%"}/>
            </Grid>
            <Grid item xs={12} md={6}>
                <h4>Progress Bar striped</h4>
                <ProgressBar progressclassName={"bg-info progress-bar-striped"} width={"25%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-success progress-bar-striped"} width={"50%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-warning progress-bar-striped"} width={"75%"}/>
                <br/>
                <ProgressBar progressclassName={"bg-danger progress-bar-striped"} width={"100%"}/>
                </Grid>
            <Grid item xs={12} md={6}>
                <h4>Progress Bar with value</h4>
                <ProgressBar progressclassName={"bg-info"} width={"25%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-success"} width={"50%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-warning"} width={"75%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-danger"} width={"100%"} type="valuebar"/>
            </Grid>
            <Grid item xs={12} md={6}>
                <h4>Progress Bar striped with value</h4>
                <ProgressBar progressclassName={"bg-info progress-bar-striped"} width={"25%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-success progress-bar-striped"} width={"50%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-warning progress-bar-striped"} width={"75%"}
                type="valuebar"/>
                <br/>
                <ProgressBar progressclassName={"bg-danger progress-bar-striped"} width={"100%"} type="valuebar"/>
            </Grid>
        </Grid>
        </div>
        ,
    );