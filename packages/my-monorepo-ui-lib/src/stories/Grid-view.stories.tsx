import { storiesOf } from "@storybook/react";
import React from "react";
import { GridView , Cards} from "../components";

storiesOf("GridView", module)
  .add("GridView",
    () =>
    <div>
        <h2>Grid-View</h2>
  <GridView  container direction="row-reverse" alignItems="baseline">
            <GridView item xs={12} >
            <Cards className="gridview-test" >xs=12</Cards>
            </GridView>
            <GridView item xs={12} md={6} >
            <Cards className="gridview-test" >xs=12 md=6 col1</Cards>
            </GridView>
            <GridView item xs={12} md={6}>
            <Cards className="gridview-test" >xs=12 md=6 col2</Cards>
            </GridView>
            <GridView item xs={12} sm={6}>
            <Cards className="gridview-test" >xs=12 sm=6</Cards>
            </GridView>
            <GridView item xs={12} sm={6}>
            <Cards className="gridview-test" >xs=12 sm=6</Cards>
            </GridView>
            <GridView item xs={6} sm={3}>
            <Cards className="gridview-test" >xs=6 sm=3</Cards>
            </GridView>
            <GridView item xs={6} sm={3}>
            <Cards className="gridview-test" >xs=6 sm=3</Cards>
            </GridView>
            <GridView item xs={6} sm={3}>
            <Cards className="gridview-test" >xs=6 sm=3</Cards>
            </GridView>
            <GridView item xs={6} sm={3}>
            <Cards className="gridview-test" >xs=6 sm=3</Cards>
            </GridView>
      </GridView>
     </div>
    );
