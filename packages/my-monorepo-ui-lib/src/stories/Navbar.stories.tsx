// import { action } from "@storybook/addon-actions";
import { withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import {Navbar} from "../components";
// import { withTests } from '@storybook/addon-jest';

const nav = [
    {
        name: "Home",
        url: "/",
    }, {
        name: "About",
        url: "/about",
    },
    {
        name: "Services",
        url: "/services",
    },
    {
        name: "Contact",
        url: "/contact",
    }, {
        name: "Help",
        url: "/help",
    },
];
const logo = "https://www.innodeed.com/wp-content/uploads/2017/11/logo_colored.png";
storiesOf("Navbar", module)
    .addDecorator(withKnobs)
    .add("basic navbar",
        () => (
            <div>
                <h4>Basic Navbar</h4>
                <Navbar altName="Logo" css="wms-ui-logo-size" src={logo} lists={nav}/>
            </div>
        ),

    );
