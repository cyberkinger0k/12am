import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { boolean, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Button, DialogBox } from "../components";

storiesOf("DialogBox", module)
    .addDecorator(withKnobs)
    .add("Second", () => (
        <DialogBox
        open={boolean("open", true)}
        fullWidth={boolean("fullWidth", true)}
        maxWidth={"lg"}
        title={"Heading............."}
        content={"Content......................................................................"}
        >    
                <Button>
                    Disagree
                </Button>
                <Button>
                    Agree
                </Button>
        </DialogBox>
    ));
