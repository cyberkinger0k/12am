import Grid from "@material-ui/core/Grid";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Avatar } from "../components";

storiesOf("Avatars", module)
.add("Avatar", () =>
<div>
<h3>Image Avatar</h3>
<Grid container={true}>
    <Avatar className="avatar-1" id="avatar"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"/>
    <Avatar className="avatar-1" id="BigAvatar"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"/>
</Grid>

<h3>Letter Avatar</h3>
<Grid container={true} >
    <Avatar className="avatar-1" id="avatar">H</Avatar>
    <Avatar className="avatar-1" id="orangeAvatar" >N</Avatar>
    <Avatar className="avatar-1" id="purpleAvatar" >OP</Avatar>
</Grid>

</div>,
);
