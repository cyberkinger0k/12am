import { storiesOf } from "@storybook/react";
import React from "react";
import { TabsComponent,TabComponent } from "../components/tabs";

storiesOf("Tabs", module)
    .add("Tabs",
        () => <TabsComponent 
                value={"devesh"}
                orientation= 'horizontal'
                variant="standard">
                    <TabComponent
                label={"1"}
                value={"1"}
                // onClick={}
                >
                </TabComponent>
                <TabComponent
                value={"devesh"}
                label={"1"}
                onClick={()=>{alert(this.label)}}
                >
                </TabComponent>
                <TabComponent
                label={"1"}
                value={"devesh !"}
                onClick={()=>{alert(this.label)}}
                >
                </TabComponent>

            </TabsComponent>,
    );
