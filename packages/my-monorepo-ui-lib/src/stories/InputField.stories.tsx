import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { InputField } from "../components";
const dt = new Date();
        const date = dt.setMonth( dt.getMonth() + 3);
let msg:string;
const error = (err: string) =>{
  console.log(err);
  msg=err;
}
storiesOf("InputFields", module)
  .addDecorator(withKnobs)
  .add("Inputs",
    () => <div>
     <h3>Standard</h3>
     <InputField type="Number"  label="Age"/>  &nbsp;&nbsp;&nbsp;&nbsp;
     <InputField type="Number" name="Age"  value="23"  label="Age"/><br/><br/>
     
     <h3>Date</h3>
     <InputField type="date & time"  dateLabel="date" timeLabel="time" margin="normal"/> 

     <h3>Outlined</h3>
     <InputField type="text"  variant="outlined" label="Outlined"/>  &nbsp;&nbsp;&nbsp;&nbsp;
     <InputField type="text" name="name"  value={text("label", "Clarke")} variant="outlined" label="Outlined"/>&nbsp;&nbsp;&nbsp;&nbsp;

     <h3>Filled</h3>
     <InputField type="email" variant="filled" label="Email"/> &nbsp;&nbsp;&nbsp;&nbsp;
     <InputField type="email" variant="filled" label="Email" value="xyz@gmail.com"/>&nbsp;&nbsp;&nbsp;&nbsp;
     <InputField type="password"  variant="filled" placeholder="password"/>  &nbsp;&nbsp;&nbsp;&nbsp;
     <InputField type="password"  showPassword={true} variant="filled" value="password" label="Password"/><br/><br/>

     <h3>Disabled</h3>
     <InputField type="text" variant="outlined" label="Disabled" disabled={boolean("Disabled", true)}  /><br/><br/>

     <h3>Multiline</h3>
     <InputField label="Multiline" multiline={true} variant="outlined"/>&nbsp;&nbsp;&nbsp;&nbsp;
     <InputField label="Multiline" multiline={true} variant="outlined" value="Hello, My Name is Devesh sahu."/><br/><br/>

     <h3>Radio Button</h3>
     < InputField type="radio" color="primary" label="Male" checked={true}/>
     < InputField type="radio" color="secondary" label="Female"checked={boolean("checked", false)}/>
     < InputField type="radio" color="primary" />
     < InputField type="radio" color="secondary" label="Disabled" disabled={boolean("Disabled", true)}/><br/><br/>

     <h3>Search Field</h3>
     < InputField type="search" name="search" label="Search field" /> &nbsp;&nbsp;&nbsp;&nbsp;
     < InputField type="search" name="search" label="Search field" value="*****"/><br/><br/>

     <h3>File</h3>
     <label className="fileLabel">Browse
     < InputField type="file"  />
     </label>

     <h3>Hidden</h3>
     < InputField type="hidden" color="success" value="abc"/>  &nbsp;&nbsp;&nbsp;&nbsp;
     <h3>url</h3>
     < InputField type="url" color="success" error={(err)=>error(err)}/>  &nbsp;&nbsp;&nbsp;&nbsp;
    <h5>{msg}</h5>
     </div>,
    );
