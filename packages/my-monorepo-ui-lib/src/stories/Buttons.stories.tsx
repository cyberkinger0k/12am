import { action } from "@storybook/addon-actions";
import { boolean, text, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import {Button} from "../components";
// import { withInfo } from '@storybook/addon-info';
// const label = 'Button size';
// const options = {
//   small: "small",
//   medium: "medium",
//   large: "large"
// };
// const options = ["small", "medium", "large"];
// const defaultValue = 'medium';
// const groupId = 'BTN-GROUP-ID1';
// const btnSize = select(label, options, defaultValue, groupId);
// import { specs, describe, it } from 'storybook-addon-specifications';
// import {mount} from "enzyme";
// import expect from "expect";
// const stories = storiesOf('Button', module);

// stories.add('Hello World', function () {
//     const story =
//       <button onClick={action('Hello World')}>
//         Hello World one
//       </button>;

//     specs(() => describe('Hello World', function () {
//       it('Should have the Hello World label', function () {
//         let output = mount(story);
//         expect(output.text()).toContain('Hello World');
//       });
//     }));

//     return story;
//   });

// const results = {"numFailedTestSuites":0,"numFailedTests":0,"numPassedTestSuites":1,"numPassedTests":1,"numPendingTestSuites":0,"numPendingTests":0,"numRuntimeErrorTestSuites":0,"numTodoTests":0,"numTotalTestSuites":1,"numTotalTests":1,"openHandles":[],"snapshot":{"added":0,"didUpdate":false,"failure":true,"filesAdded":0,"filesRemoved":0,"filesRemovedList":[],"filesUnmatched":0,"filesUpdated":0,"matched":1,"total":1,"unchecked":1,"uncheckedKeysByFile":[{"filePath":"/var/www/html/wms-app/packages/wms-components/test/button.test.tsx","keys":["renders correctly 1"]}],"unmatched":0,"updated":0},"startTime":1571041412897,"success":false,"testResults":[{"assertionResults":[{"ancestorTitles":[],"failureMessages":[],"fullName":"Button should render correctly","location":null,"status":"passed","title":"Button should render correctly"}],"endTime":1571041415893,"message":"","name":"/var/www/html/wms-app/packages/wms-components/test/button.test.tsx","startTime":1571041413761,"status":"passed","summary":""}],"wasInterrupted":false};
storiesOf("Button", module)
    .addDecorator(withKnobs)
    // .addDecorator(withInfo( (story: any, context: { kind: string; story: string; })
    .add("variant",
        () => (
            <div>
                <h4>Contained Buttons</h4>
                <Button size="medium" variant="contained" btnColor="default" onClick={action("clicked")}>Default</Button>
                &nbsp;&nbsp;
                <Button variant="contained" btnColor="primary">Primary</Button>
                &nbsp;&nbsp;
                <Button disabled={boolean("Disabled", false)} variant="contained" btnColor="secondary"  onClick={action("button clicked")}>
                {text("Label", "Secondary")}
                </Button>
                <h4>Text Buttons</h4>
                <Button size="medium" disabled={true} variant="text" btnColor="default" onClick={action("clicked")}>Default</Button>
                &nbsp;&nbsp;
                <Button size="medium" variant="text" btnColor="primary" onClick={action("clicked")}>Primary</Button>
                &nbsp;&nbsp;
                <Button size="medium" variant="text" btnColor="secondary" onClick={action("clicked")}>Secondary</Button>
                <h4>Outlined Buttons</h4>
                <Button size="medium" variant="outlined" btnColor="default" onClick={action("clicked")}>Default</Button>
                &nbsp;&nbsp;
                <Button size="medium" variant="outlined" btnColor="primary" onClick={action("clicked")}>Primary</Button>
                &nbsp;&nbsp;
                <Button size="medium" variant="outlined" btnColor="secondary" onClick={action("clicked")}>Secondary</Button>
                <h4>Button sizes</h4>
                <Button size="small" variant="contained" btnColor="default" onClick={action("clicked")}>Small Button</Button>
                &nbsp;&nbsp;
                <Button size="medium" variant="contained" btnColor="default" onClick={action("clicked")}>Medium Button</Button>
                &nbsp;&nbsp;
                <Button size="large" variant="contained" btnColor="default" onClick={action("clicked")}>Large Button</Button>
                <h4>Full width button</h4>
                <Button fullWidth={true} size="large" variant="contained" btnColor="default" onClick={action("clicked")}>Full width</Button>

            </div>
        ),

    );
