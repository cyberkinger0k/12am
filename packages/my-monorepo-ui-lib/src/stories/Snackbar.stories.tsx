// import { action } from "@storybook/addon-actions";
import { boolean, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import {Snackbar} from "../components";
// import { withInfo } from '@storybook/addon-info';
const position = {
    vertical: "bottom",
    horizontal: "center",
};
const snackbarSuccess = "snackbarSuccess";
const snackbarWarning = "snackbarWarning";
const snackbarError = "snackbarError";
const snackbarInfo = "snackbarInfo";
storiesOf("Snackbar", module)
.addDecorator(withKnobs)
// .addDecorator(
//     withInfo)
.addParameters({
    info: {
      // Your settings
    },
})
.add(
    "Standard",
    () => (
        <div>
            <Snackbar css={snackbarSuccess} position={position} horizontal="center" open={boolean("open", true)}  autoHideDuration={6000} message="This is my default success message."/>
            <Snackbar css={snackbarWarning} position={{vertical: "bottom", horizontal: "left"}} horizontal="left" open={boolean("open", true)} autoHideDuration={6000} message="This is my default warning message"/>
            <Snackbar css={snackbarError} position={{vertical: "bottom", horizontal: "right"}} horizontal="left" open={boolean("open", true)} autoHideDuration={6000} message="This is my default error message"/>
            <Snackbar css={snackbarInfo} position={{vertical: "top", horizontal: "center"}} horizontal="left" open={boolean("open", true)} autoHideDuration={6000} message="This is my default info message"/>

        </div>
    ), {
        notes: "A very simple example of addon notes",
        info: {
            text: `<div>Hello world</div>`,
            inline: true,
            header: false,
         },
    },
);
