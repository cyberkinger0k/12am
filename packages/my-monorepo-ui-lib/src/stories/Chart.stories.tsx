import { storiesOf } from "@storybook/react";
import React from "react";
import { Chart } from '../components';
  const chartData = [
    {
      id: 1,
      user : {
        id: 101,
        name: "anil",
      },
      session : {
        id : 1001,
        name : "Git Essential Training",
      },
      response_value : 5,
      answers : [1,2,3,4,5,5,4,3,2,5]
    },
    {
      id: 2,
      user : {
        id: 102,
        name: "Tarni",
      },
      session : {
        id : 1001,
        name : "Git Essential Training",
      },
      response_value : 5,
      answers : [1, 2, 3, 4, 5, 5, 4, 3, 2, 5],
    },
    {
      id: 3,
      user : {
        id: 103,
        name: "Vikram",
      },
      session : {
        id : 1001,
        name : "Git Essential Training",
      },
      response_value : 2,
      answers : [4, 2, 3, 4, 3, 5, 4, 3, 2, 5],
    },
    {
      id: 4,
      user : {
        id: 104,
        name: "Kashif",
      },
      session : {
        id : 1001,
        name : "Git Essential Training",
      },
      response_value : 3,
      answers : [1, 5, 3, 4, 2, 1, 4, 3, 2, 1],
    },
    {
      id: 5,
      user : {
        id: 105,
        name: "Devesh",
      },
      session : {
        id : 1001,
        name : "Git Essential Training",
      },
      response_value : 4,
      answers : [2, 3, 3, 4, 2, 1, 4, 3, 2, 1],
    },
 
  ];
const response = [
  {key:1,value:'Very Poor'},
  {key:2,value:'Poor'},
  {key:3,value:'Average'},
  {key:4,value:'Good'},
  {key:5,value:'Excellent'}
];
const response2 =['Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8','Q9','Q10']
  storiesOf("Charts",module)
  .add("charts", () =>
    <div> 
    {/* <h2>Line Chart</h2>
    <Chart  type="line" name="Overall Review" className="chart-1" id="chart-1" data={chartData} width={500} height={300}
        margin={{ top: 5, right: 30, left: 20, bottom: 5,}} xKey={'response_value'} response={response}>
      </Chart> */}
  
    <h2>Overall Review</h2>
    <Chart type="bar" name="Overall Review" className="chart-2" id="chart-2" data={chartData} width={700} height={300}
        margin={{ top: 5, right: 30, left: 20, bottom: 5,}}  xKey={'response_value'}  response={response}>
            
    </Chart>
    <h2>Bar Chart</h2>
    <Chart type="bar"name="Feedbacks" className="chart-3" id="chart-3" data={chartData} width={700} height={300}
        margin={{ top: 5, right: 30, left: 20, bottom: 5,}}  xKey={'answers'}  response={response2} >
           
    </Chart>
    </div>
  );