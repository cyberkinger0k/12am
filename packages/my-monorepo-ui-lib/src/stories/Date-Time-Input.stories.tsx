import { storiesOf } from "@storybook/react";
import React from "react";
import { InputField} from "../components";

storiesOf("Date-Time", module)
    .add("Date-Time", () => (
        <InputField name='time' type="time" variant="outlined"
        label="Time: "/> 
    ));
