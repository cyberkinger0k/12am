import {withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { SelectBox } from "../components";
const list = [
    {   id: 1,
        value: "one",
    },
    {
        id: 2,
        value: "two",
    },
    {
        id: 3,
        value: "Three",
    },
];

storiesOf("SelectBox", module)
  .addDecorator(withKnobs)
  .add("SelectBox",
    () =>
    <div>
        <h3>Simple Select</h3>
        <SelectBox className="select" id="select" value={list} name="Options"/><br/><br/>

        <h3>Without label</h3>
        <SelectBox className="select" id="select" value={list} /><br/><br/>

        <h3>Placeholder</h3>
        <SelectBox className="select" id="select" value={list} placeholder="None"/><br/><br/>

        <h3>Disabled</h3>
        <SelectBox className="select" id="select" value={list} name="Options" disabled={true} /><br/><br/>

        <h3>Label + Placeholder</h3>
        <SelectBox className="select" id="select" value={list} name="Options"  placeholder="None"/><br/><br/>

     </div>,
    );
