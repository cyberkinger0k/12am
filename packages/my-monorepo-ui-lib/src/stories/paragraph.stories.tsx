import { text, withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { P } from "../components";

storiesOf("Paragraph", module)
    .addDecorator(withKnobs)
    .add("Paragraph",
        () => <P id="paragraph-1" className="paragraph-1">Hello, My name is Chris Hemsworth.{text("value", "")}</P>,
    );
