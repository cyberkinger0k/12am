import {withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { DropDown } from "../components";

const click = () => {
    alert("hello");
}
const list = [
    {   id: 1,
        value: "Option 1",
        onclick: click,
    },
    {
        id: 2,
        value: "Option 2",
        onclick: click,
    },
    {
        id: 3,
        value: "Option 3",
        onclick: click,
    },
    {
        id: 4,
        value: undefined,
        onclick: click,
    },
];


storiesOf("Dropdown", module)
  .addDecorator(withKnobs)
  .add("Dropdown",
    () =>
    <div>
        <h3>DropDown</h3>
        <DropDown className="dropdown" value={list}  label="DropDown"/>
        <DropDown className="dropdown" value={list}  label="DropDown"/>
        <DropDown className="dropdown" value={list}  label="DropDown" type="icon"/>
     </div>
    );
