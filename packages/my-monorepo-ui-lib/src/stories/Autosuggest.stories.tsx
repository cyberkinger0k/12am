import { storiesOf } from "@storybook/react";
import React from "react";
import { AutoSuggest, TypographyComponent } from "../components";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Options } from "@material-ui/core/useMediaQuery";
import { TextField } from "@material-ui/core";

const options = [
  { value: 'The Shawshank Redemption', year: 1994 },
  { value: 'The Godfather', year: 1972 },
  { value: 'The Godfather: Part II', year: 1974 },
  { value: 'The Dark Knight', year: 2008 },
  { value: '12 Angry Men', year: 1957 },
  { value: "Schindler's List", year: 1993 },
  { value: 'Pulp Fiction', year: 1994 },
  { value: 'The Lord of the Rings: The Return of the King', year: 2003 },
  { value: 'The Good, the Bad and the Ugly', year: 1966 },
  { value: 'Fight Club', year: 1999 },
  { value: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
  { value: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
  { value: 'Forrest Gump', year: 1994 },
  { value: 'Inception', year: 2010 },
  { value: 'The Lord of the Rings: The Two Towers', year: 2002 },
  { value: "One Flew Over the Cuckoo's Nest", year: 1975 },
]

const mystyle = {
  paddingTop: "10px"
};
storiesOf("Autosuggest", module)
.add("Autosuggest", () =>
  <div>
    <h4>Simple AutoSuggest</h4>
    <AutoSuggest
    id="combo-box-demo"
    options={options}
    optionLabel={(option)=> option.value}
    label="simple"
    />

    <h4>Auto suggestion outlined</h4>
    <AutoSuggest
    id="combo-box-demo"
    options={options}
    optionLabel={(option)=> option.value}
    variant="outlined"
    className={mystyle}
    label="outlined"
    />

    <h4>Clear on escape</h4>
    <AutoSuggest
    id="combo-box-demo"
    options={options}
    optionLabel={(option)=> option.value}
    variant="outlined"
    className={mystyle}
    label="Clear on escape"
    clearOnEscape={true}
    />

    <h4>Free solo input</h4>
    <AutoSuggest
    id="combo-box-demo"
    options={options}
    optionLabel={(option)=> option.value}
    variant="outlined"
    className={mystyle}
    label="Free solo input"
    freeSolo={true}
    />

    <h4>Disable</h4>
    <AutoSuggest
    id="combo-box-demo"
    options={options}
    optionLabel={(option)=> option.value}
    variant="outlined"
    className={mystyle}
    label="disable"
    freeSolo={true}
    disabled={true}
    />
  </div>
    // <Autocomplete
    //   id="combo-box-demo"
    //   options={options}
    //   getOptionLabel={(option: any) => option.value}
    //   style={{ width: 300 }}
    //   renderInput={params => (
    //     <TextField {...params} label="Combo box" variant="outlined" fullWidth />
    //   )}
    // />
      );
