import { storiesOf } from "@storybook/react";
import React from "react";
import { ButtonLoader, Button } from "../components";

storiesOf("Button loader", module)
.add("Button loader", () =>
  <div>
  <h3>Button loader circular</h3>
    <Button size="medium" variant="outlined" btnColor="default">
    <ButtonLoader type="circular"></ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="default">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="primary">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="secondary">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>
    <br /><br />
    <Button size="medium" variant="contained" btnColor="default">
    <ButtonLoader type="circular"></ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="default">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="primary">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="secondary">
    <ButtonLoader type="circular">loading....</ButtonLoader>
    </Button>

  <h3>Button loader grow</h3>
  <Button size="medium" variant="outlined" btnColor="default">
    <ButtonLoader type="grow"></ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="default">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="primary">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="outlined" btnColor="secondary">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
    <br /><br />
    <Button size="medium" variant="contained" btnColor="default">
    <ButtonLoader type="grow"></ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="default">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="primary">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
    &nbsp;&nbsp;
    <Button size="medium" variant="contained" btnColor="secondary">
    <ButtonLoader type="grow">loading....</ButtonLoader>
    </Button>
  </div>,
);

