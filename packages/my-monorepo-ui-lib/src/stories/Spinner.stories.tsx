import { withKnobs} from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import {Spinner} from "../components";
storiesOf("Spinner", module)
    .addDecorator(withKnobs)
    // .addDecorator(withInfo( (story: any, context: { kind: string; story: string; })
    .add("Spinner Option",
        () =>
        <div>
            <Spinner color="secondary"/>
            <Spinner color="primary"/>
         </div>,
    );
