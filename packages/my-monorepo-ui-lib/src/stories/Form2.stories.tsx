import React from "react";
import { storiesOf } from "@storybook/react";
import { Form, FormInput} from '../components/form2';
import { withKnobs, date} from '@storybook/addon-knobs';
import { TextField } from "@material-ui/core";

const inputStyle = {
    width:1000
}
export const colourOptions = [
    { name: 'ocean', label: 'Ocean'},
    { name: 'blue', label: 'Blue'},
  ];
storiesOf("Form2", module)
    .addDecorator(withKnobs)
    .add("Example",
        () => 
        <Form onSubmit={values => { console.log(values) }}>
        
        <FormInput layout={3} name='session' type="text"  variant="outlined" label="Session"/>
        <br />
        <FormInput layout={3} name='title' type="text"  variant="outlined" 
        label="Title"/>
        <br />
        <FormInput layout={3} name='presentedBy' type="selectBox"  variant="outlined" 
        label="Presented By" value={['1','2','3']}/>
        <br />
        <FormInput layout={3} name='designation' type="text" variant="outlined"
        label="Designation" value="S/W Engineer" disabled={true}/>
        <br />
        <FormInput layout={3} name='date' type="date" variant="outlined"
        label="Date" value={Date.now()} disabled={true}/>
        <br />
        <FormInput layout={3} name='time' type="time" variant="outlined"
        label="Time"/>
        <br />
        <FormInput layout={3} name='datetime' type="date & time" required={true} variant="outlined"
        label="Date & Time  "/>
        <FormInput layout={3} name='chip' label="chip" type={""} isMulti={true}
        options={colourOptions} value={colourOptions}/>
        <button>Submit</button>
        </Form>
    )