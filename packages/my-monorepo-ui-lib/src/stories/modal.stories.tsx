import { boolean, withKnobs } from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import React from "react";
import { ModalComponent } from "../components";

storiesOf("ModalComponent", module)
    .addDecorator(withKnobs)
    .add("ModalComponent",
        () =>
        
        <ModalComponent
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={boolean("open", true)}
        header={"Text in a modal"}
        ><div>hello</div></ModalComponent>
    );
