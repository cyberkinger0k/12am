import React , { Component } from "react";
import { ListItemSecondaryAction } from "@material-ui/core";

export interface ListButtonProps {
    id?: string;
    className?: string;
}
export class ListButton extends Component<ListButtonProps, {}> {

    public render() {

        return (
            <ListItemSecondaryAction className={this.props.className}>
            {this.props.children}
          </ListItemSecondaryAction>
        );
    }
}