import React , { Component } from "react";
import { ListItem, ListItemText, ListItemAvatar, ListItemSecondaryAction } from "@material-ui/core";
import { Avatar } from "../avatar";
// import ImageIcon from '@material-ui/icons/Image';
// import WorkIcon from '@material-ui/icons/Work';
// import BeachAccessIcon from '@material-ui/icons/BeachAccess';

export interface ListItemProps {
    id?: string;
    className?: string;
    primaryContent:string;
    avatarsrc?:string;
    avatardata?:string;
    secondaryContent?:string;
    avatarclassName?:string;
}
export class ListItems extends Component<ListItemProps, {}> {

    public render() {

        return (
                <ListItem>
                {this.props.avatarsrc || this.props.avatardata ?
                  <ListItemAvatar>
                        <Avatar className={this.props.avatarclassName} src={this.props.avatarsrc}>
                        {this.props.avatardata}
                        </Avatar>
                  </ListItemAvatar>
                :null }
                  <ListItemText
                    primary={this.props.primaryContent}
                    secondary={this.props.secondaryContent}
                  />
                  {this.props.children}
                </ListItem>
        );
    }
}