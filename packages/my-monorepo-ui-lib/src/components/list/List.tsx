import React , { Component } from "react";
import { List } from "@material-ui/core";
// import ImageIcon from '@material-ui/icons/Image';
// import WorkIcon from '@material-ui/icons/Work';
// import BeachAccessIcon from '@material-ui/icons/BeachAccess';

export interface ListProps {
    children?: React.ReactNode;
    id?: string;
    className?: string;
}
export class Li extends Component<ListProps, {}> {

    public render() {

        return (
            <List className={this.props.className}>
                {this.props.children}
            </List>
        );
    }
}