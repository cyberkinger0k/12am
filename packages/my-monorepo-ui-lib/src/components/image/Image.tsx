import React from "react";

export interface ImageProps {
    className?: string;
    src?: string;
    altName?: string;
}

export const Image: React.FC<ImageProps> = (props: React.PropsWithChildren<ImageProps>) => {
        const {className, src, altName} = props;
        return  <img className={className} src={src} alt={altName}/>;
};
