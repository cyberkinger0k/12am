import React, { Component } from "react";
import "./InputField.scss";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import "date-fns";
import { FaEye, FaEyeSlash,FaClock} from 'react-icons/fa';
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { getMilliseconds } from "date-fns";
import { InputLabel, Input, InputAdornment, IconButton, FormControl, OutlinedInput, Icon } from "@material-ui/core";

export interface InputProps{
    onChange?: (value?: any, name?: any) => any;
    handleClickShowPassword?: () => any;
    className?: string;
    class?: string;
    name?: string;
    rowsMax?: number;
    type?: string;
    placeholder?: string;
    required?: boolean;
    value?: any;
    variant?: any;
    label?: string;
    disabled?: boolean;
    multiline?: boolean;
    color?: string;
    checked?: boolean;
    id?: string;
    format?: string;
    timeLabel?: string;
    dateLabel?: string;
    margin?: any;
    maxDate?:Date
    selectedDate?:any
    showPassword?:boolean;
    error?:any;
    defaultValue?:any;
    errorShow?:boolean;
    helperText?:string;
}


export class InputField extends Component<InputProps, {}> {
   public state = {
      selectedDate: new Date(),
    };
    public render() {
        const dt = new Date();
        const date = dt.setMonth( dt.getMonth() + 3);
        const props = this.props;
        if (props.type === "radio") {
            return(
                <FormControlLabel className={`wms-ui-InputField ${this.props.className}`} id={`wms-ui-InputField ${this.props.id}`} 
                   value={this.props.value} control={<Radio />} label={this.props.label} disabled={this.props.disabled} checked={this.props.checked} />
            );
        }
        if (props.type === "simple") {
        return(
            <div className={props.class}>
                <input name={props.name} type={this.props.type} className={`form-control ${this.props.className}`} placeholder={props.placeholder}
                value={props.value} onChange={this.handleChange} required={props.required}/>
            </div>
    
            );
        }
        if(props.type === 'selectBox'){
            return(
                <select className={`browser-default custom-select form-control ${this.props.className}`} name={this.props.name}
                    onChange={this.handleChange}>
                    <option disabled selected>{this.props.name}</option>
                    {this.props.value.map((value1: any) =>
                        <option value={value1.id || value1}>{value1.firstName || value1}</option>
                    )}
                </select>
                );
            }
            if(props.type === 'date & time'){
                return(
               <MuiPickersUtilsProvider utils={DateFnsUtils}>
                 <KeyboardDatePicker
                   className={this.props.className}
                   id={this.props.id}
                   format ="dd-MM-yyyy"
                   label={this.props.dateLabel}
                   value={this.props.selectedDate ? this.props.selectedDate : this.state.selectedDate}
                   onChange={this.handleDateChange}
                   margin={this.props.margin}
                   minDate={this.state.selectedDate}
                   maxDate={date}
                 /> &nbsp;&nbsp; 
                 <KeyboardTimePicker
                   id={this.props.id}
                   className={this.props.className}
                   label={this.props.timeLabel}
                   value={this.props.selectedDate ? this.props.selectedDate : this.state.selectedDate}
                   onChange={this.handleDateChange}
                   margin={this.props.margin}
                   KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  keyboardIcon={<FaClock />}
                />
                 </MuiPickersUtilsProvider>
                 ); }
         if(props.type === 'password'){
             return(
                <div>
                    <FormControl className={this.props.className} variant="outlined">
                    <OutlinedInput
                        id="outlined-adornment-weight"
                        name={props.name}
                        type={props.showPassword ? 'text' : 'password'}
                        value={props.value}
                        onChange={this.handleChange}
                        placeholder={props.placeholder}
                        endAdornment={
                        <InputAdornment position="end">
                        <IconButton
                        aria-label="toggle password visibility"
                        onClick={this.handleClickShowPassword}
                        edge="end"
                        >
                        {props.showPassword ? <FaEye /> : <FaEyeSlash />}
                        </IconButton>
                        </InputAdornment>}
                        aria-describedby="outlined-weight-helper-text"
                        inputProps={{
                        'aria-label': 'weight',
                        }}
                        labelWidth={0}
                    />
                    </FormControl>
                 {/* <FormControl className={this.props.className} variant="outlined">
                     {this.props.label?
             <InputLabel htmlFor="outlined-adornment-password">{this.props.label}</InputLabel>:null}
                    <OutlinedInput
                        name={props.name} 
                        id="outlined-adornment-password"
                        type={props.showPassword ? 'text' : 'password'}
                        value={props.value}
                        onChange={this.handleChange}
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            edge="end"
                            >
                            {props.showPassword ? <FaEye /> : <FaEyeSlash />}
                            </IconButton>
                        </InputAdornment>
                        }
                        labelWidth={70}
                    />
                 </FormControl> */}
                </div>
             );
            }
        else {
            return(
                <TextField type={this.props.type} className={`wms-ui-InputField ${this.props.className}`} id={`wms-ui-InputField ${this.props.id}`} 
                 name={props.name}  placeholder={props.placeholder} value={props.value} rowsMax={props.rowsMax} onChange={this.handleChange} required={props.required} defaultValue={props.defaultValue}
                 variant={props.variant} multiline={props.multiline} disabled={props.disabled} label={props.label} error={this.props.helperText ? true : false} helperText={this.props.helperText}/>
                 
                );
        }
    }
private validateurl(str:string)
{ 
  /* this exp is valid for"http://www.example.com","https://www.example.com","www.example.com" */
  const regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str))
        {
          return true;
        }
        else
        {
          return false;
        }
}
    private handleChange = (e: any) => {
        this.props.onChange && this.props.onChange(e.target.value, e.target.name);
        if(this.props.type === "url"){
            if(!this.validateurl(e.target.value)){
                this.props.error("please enter valid URL");
            }
        }
     }
   private handleDateChange = (e: any) => {
       this.setState({selectedDate: e})
       this.props.onChange && this.props.onChange(e, 'date & time');
   }
  private handleClickShowPassword = () => {
    this.props.handleClickShowPassword && this.props.handleClickShowPassword();
  };

 }