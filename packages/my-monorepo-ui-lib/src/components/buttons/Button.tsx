import { Button as MButton } from "@material-ui/core";
import React from "react";

export interface ButtonProps {
    className?: string;
    btnColor?: "inherit" | "primary" | "secondary" | "default" | undefined;
    variant?: "contained" | "text" | "outlined" | undefined;
    size?: "small" | "medium" | "large" | undefined;
    disabled?: boolean;
    fullWidth?: boolean;
    startIcon?: any;
    endIcon?: any;
    onClick?: (btnColor: any) => void;
    onKeyPress?: (event: any) => void;
    type?: "button" | "submit" | "reset";
    name?: string;
    // onClick?: (e:any) => void
}

export const Button: React.FC<ButtonProps> = (props: React.PropsWithChildren<ButtonProps>) => {
    const { btnColor, className, children, variant, onClick, size, disabled, fullWidth, startIcon, endIcon, type, name, onKeyPress } = props;
    return <MButton className={`wms-ui-button ${className}`} disabled={disabled} size={size}
        color={btnColor} variant={variant} onClick={() => onClick && onClick(btnColor)}
        fullWidth={fullWidth} startIcon={startIcon} endIcon={endIcon} type={type} name={name} onKeyPress={onKeyPress}>
        {children}
    </MButton>;
};
