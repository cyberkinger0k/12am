import React from "react";
import {Dialog} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Button } from "..";
import "./DialogBox.scss";

export interface DialogBoxProps {
    children?: React.ReactNode;
    className?: string;
    id?: string;
    open: boolean;
    fullWidth?: boolean;
    maxWidth?: "xs"| "sm"| "md"| "lg"|"xl";
    title?: string;
    content?: string;
    confirmButton?: string;
    cancelButton?: string;
     onClick1?: (e:any) => void;
     onClick2?: (e:any) => void;
     showDefaultButton?: boolean;
     buttonClass?: string;
     defaultButtonVariant?: "contained" | "text" | "outlined" |undefined;
     defaultButton1Color?:"inherit" | "primary" | "secondary" | "default" | undefined;
     defaultButton2Color?:"inherit" | "primary" | "secondary" | "default" | undefined;
}

export const DialogBox: React.SFC<DialogBoxProps> = (props) => {
    let action;
    if(props.showDefaultButton){
    action = (
      <DialogActions>
    <Button variant={props.defaultButtonVariant} className={`dialog-button`} btnColor={props.defaultButton1Color} onClick = {props.onClick2}>{props.cancelButton?props.cancelButton:"No"}</Button>
    <Button variant={props.defaultButtonVariant} className={`dialog-button ${props.buttonClass}`} btnColor={props.defaultButton2Color} onClick = {props.onClick1}>{props.confirmButton?props.confirmButton:"Yes"}</Button>
    {props.children}
    </DialogActions>  
    )
  } else
  { action = (
    <DialogActions>
    {props.children}
    </DialogActions>
  )}
  return <Dialog id={props.id} className={`wms-ui-dialogBox ${props.className}`} open={props.open}
    fullWidth={props.fullWidth} maxWidth={props.maxWidth}>
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
          <DialogContentText>
           {props.content}
          </DialogContentText>
        </DialogContent>
        {action}
    </Dialog>
 }
