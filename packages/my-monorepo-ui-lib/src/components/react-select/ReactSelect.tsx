import React, { Component } from 'react';
import Select from "react-select";
import './ReactSelect.scss';


export interface ReactSelectProps {
    onChange?: (value?: any, name?: any) => any;
    value?: any;
    className?: string;
    name?: string;
    options?:any;
    isMulti?:boolean;
    defaultValue?:any;
    id?: string
    placeholder?:string;
    required?: boolean;
    helperText?: string;
}
export class ReactSelect extends Component<ReactSelectProps, {}>{


   
    render() {
        return (
            <div>
            <Select
            onChange={this.props.onChange}
            isMulti={this.props.isMulti}
            name={this.props.name}
            value={this.props.value}
            defaultValue={this.props.defaultValue}
            options={this.props.options}
            className={!this.props.helperText ? `basic-multi-select ${this.props.className}` : `basic-multi-select error ${this.props.className}`}
            classNamePrefix="select"
            id={this.props.id}
            required={this.props.required}
            placeholder={this.props.placeholder}
            />{this.props.helperText && 
            <p className="msg-error ">{this.props.helperText && this.props.helperText}</p>}
            </div>

        )
    }
}