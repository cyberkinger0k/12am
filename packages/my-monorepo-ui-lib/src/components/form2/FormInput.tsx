import React, { useImperativeHandle } from 'react';
//import PropTypes from "prop-types";
import { FormConsumer } from './Form';
import { InputField, Label } from '..';
import "./Form.scss";
import { ReactSelect } from '../react-select/ReactSelect';

interface IformProps {
    labelclassname?: string;
    inputClassName?: string;
    placeholder?: string;
    required?: boolean;
    multiline?: boolean;
    variant?: string;
    disabled?: boolean;
    requiredField?: boolean;
    Inputlabel?: string;
    children?: any;
    label?: string;
    layout: number;
    name: string;
    value?: any;
    type: string;
    options?: any;
    chipDiv?: string;
    isMulti?: boolean;
    defaultValue?: any;
    format?: string;
    timeLabel?: string;
    dateLabel?: string;
    margin?: any;
    id?: string;
    selectedDate?:any;
    chipVariant?: "outlined" | "standard" | "filled";
    onChange?: (name: string, e: any) => any;
}
let yourChips: any =[];
export const FormInput = (props: IformProps) => (
    <FormConsumer>
        {({ values, setValue }) => {
            if (props.layout === 1) {
                return (
                    <div className="div-margin">
                        <Label className={`${props.labelclassname} form-labels`} content={props.label && props.label} required={props.requiredField}></Label>
                        <InputField
                            name={props.name}
                            type={props.type}
                            className={`wms-ui-InputField form-inputfield ${props.inputClassName}`}
                            placeholder={props.placeholder}
                            required={props.required}
                            variant={props.variant}
                            multiline={props.multiline}
                            disabled={props.disabled}
                            label={props.Inputlabel}
                            value={props.value}
                            margin={props.margin}
                            selectedDate={props.selectedDate}
                            id={props.id}
                            onChange={(event) => {
                                //event.preventDefault();
                                setValue(props.name, event);
                                {props.onChange? props.onChange(props.name, event):null};
                                
                            }}
                        />
                    </div>
                );
            }
            if (props.layout === 2) {
                return (
                    <div className="div-margin">
                        <Label className={props.labelclassname} content={`${props.label}`} required={props.requiredField}>
                        </Label><br />
                        <InputField
                            name={props.name}
                            type={props.type}
                            className={`wms-ui-InputField ${props.inputClassName}`}
                            placeholder={props.placeholder}
                            required={props.required}
                            variant={props.variant}
                            multiline={props.multiline}
                            disabled={props.disabled}
                            label={props.Inputlabel}
                            value={props.value}
                            onChange={(event) => {
                                //event.preventDefault();
                                setValue(props.name, event);
                                {props.onChange? props.onChange(props.name, event):null};
                            }}
                        />
                    </div>
                );
            }
            if (props.layout === 3) {
                return (
                    <div className={`${props.chipDiv} div-margin`}>
                        <Label className={`${props.labelclassname} form-labels`} content={`${props.label}`} required={props.requiredField}>
                        </Label>
                        <ReactSelect
                            className={`${props.inputClassName} form-inputfield`}
                            isMulti={props.isMulti}
                            name={props.name}
                            onChange={(event) => {
                                //event.preventDefault();
                                setValue(props.name, event);
                                {props.onChange? props.onChange(props.name, event):null};
                            }}
                            defaultValue={props.defaultValue}
                            placeholder={props.placeholder}
                            options={props.options}
                            value={props.value}
                            />
                    </div>
                );
            }

        }}
    </FormConsumer>
);

// Input.propTypes = {
//     name: PropTypes.string.isRequired
// };