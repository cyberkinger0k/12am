import React, { Component } from "react";
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from "@material-ui/core";

export interface IPropsAutoSuggest {
    children?: any;
    content?: string;
    className?: any;
    id?: string;
    options: any;
    variant?:"outlined";
    optionLabel: any;
    label?: any;
    clearOnEscape?:boolean;
    freeSolo?:boolean;
    disabled?:boolean;
}
export class AutoSuggest extends Component<IPropsAutoSuggest, {}> {
    state={
        values:"",
        inputValues:"",
    }
    public render() {

        return (
            <Autocomplete
            className={this.props.className}
            options={this.props.options}
            getOptionLabel={this.props.optionLabel}
            style={{ width: 300 }}
            renderInput={params => (
                <TextField {...params} label={this.props.label} variant={this.props.variant} fullWidth 
               />
            )}
            clearOnEscape={this.props.clearOnEscape}
            freeSolo={this.props.freeSolo}
            inputValue={this.state.values}
            disabled={this.props.disabled}
            // value={this.state.values}
            onInputChange={this.onChange}
            onChange={this.onChange}
            />
            )}
    // private onInputChange = (e,inputValue) => {
    //     this.setState({inputValues: inputValue});
    // }
    private onChange = (e,value) => { 
        console.log(e);
         if ((e === null && value === "") || (e.target.value && value === "")){
             return false;
         }
        if(value === null){
            this.setState({values:""})
        }else if (value.value){
            this.setState({values: value.value});
            // this.setState({inputValues:value.title});
        }else if(value || value === ""){
            this.setState({values:value});
        }
    }
}
