import React, { Component }  from "react";
import Grid from "@material-ui/core/Grid";
import "./Grid-View.scss";

export interface IGridProps{
        children?: React.ReactNode;
        className?: string,
        item?: boolean,
        container?: boolean,
        sm?: false| 'auto' | true | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12,
        md?: false| 'auto' | true | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12,
        lg?: false| 'auto' | true | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12,
        xs?: false| 'auto' | true | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12,
        xl?: false| 'auto' | true | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12,
        justify?: 'flex-start'| 'center'| 'flex-end'| 'space-between'| 'space-around'| 'space-evenly',
        direction?:'row' | 'row-reverse' | 'column' | 'column-reverse',
        alignItems?: 'flex-start'| 'center' | 'flex-end' | 'stretch' | 'baseline'
}

export class GridView extends Component<IGridProps,{}>{

    render()
    {
        const props = this.props;
        return(
            <Grid className={props.className} sm={props.sm} md={props.md} lg={props.lg}  xs={props.xs} xl={props.xl} item={props.item}
               container={props.container} justify={props.justify} direction={props.direction} alignItems={props.alignItems}>
             {props.children}
            </Grid>
        );
    }
}