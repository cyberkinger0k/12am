export class SocialLoginCommon {
static sdk = (src:string,jssdk:string) => {
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = src;
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', jssdk));
};
}