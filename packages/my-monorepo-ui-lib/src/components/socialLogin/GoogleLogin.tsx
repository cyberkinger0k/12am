import React from 'react';
import "./SocialLogin.scss";
import { SocialLoginCommon } from './SocialLoginCommon';
import { Button } from '../';

interface IGoogleLoginProps {
    clientId:string;
    scope:string;
    className?:string;
}
export class GoogleLogin extends React.Component<IGoogleLoginProps,{}> {

    componentDidMount() {
        this.googleSDK();
        console.log('sfsfd');
    }

    googleSDK = () => {
        let client_id= this.props.clientId;
        let scope = this.props.scope;
        window['googleSDKLoaded'] = () => {
          window['gapi'].load('auth2', () => {
            const auth2 = window['gapi'].auth2.init({
              client_id: client_id,
              cookiepolicy: 'single_host_origin',
              scope: scope
            });
            this.prepareLoginButton(auth2);
          });
        }
        SocialLoginCommon.sdk("https://apis.google.com/js/platform.js?onload=googleSDKLoaded","google-jssdk");
        // (function(d, s, id){
        //   let js, fjs = d.getElementsByTagName(s)[0];
        //   if (d.getElementById(id)) {return;}
        //   js = d.createElement(s); js.id = id;
        //   js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
        //   fjs.parentNode.insertBefore(js, fjs);
        // }(document, 'script', 'google-jssdk'));
     
    }

    prepareLoginButton = (auth2) => {
 
    console.log(this.refs.googleLoginBtn);
 
        auth2.attachClickHandler(this.refs.googleLoginBtn, {},
        (googleUser) => {
 
        let profile = googleUser.getBasicProfile();
        console.log('Token || ' + googleUser.getAuthResponse().id_token);
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
        //YOUR CODE HERE
 
 
        }, (error) => {
            alert(JSON.stringify(error, undefined, 2));
        });
 
    }
 
    
   
    render() {
 
        return (    
                <Button className={`loginBtn loginBtn--google ${this.props.className}`}
                size="medium" variant="contained" onClick={this.prepareLoginButton}>
                    Login with Google
                </Button>       
        );
    }
}
