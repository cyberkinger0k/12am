import React from 'react';
import "./SocialLogin.scss";
import { SocialLoginCommon } from './SocialLoginCommon';
import { Button } from '../buttons';

export interface IFacebookLoginState {
  fb:any;
}
export interface IFacebookLoginProps {
  appId:string;
  className?:string;
}
declare global {
  interface Window { fbAsyncInit: any; }
 
}
export class FacebookLogin extends React.Component<IFacebookLoginProps,IFacebookLoginState> {
  public FB: any = null;
  constructor(props) {
    super(props);
    this.state = {
        fb:{}
    };
  }

  loadFbLoginApi = (parentThis) => {
        let appId = this.props.appId
        window.fbAsyncInit = function() {
          parentThis.setState({fb:this.FB});
          this.FB.init({
                appId      : appId,
                cookie     : true,  // enable cookies to allow the server to access
                // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.5' // use version 2.1
            });
        };
          // Load the SDK asynchronously
          SocialLoginCommon.sdk("//connect.facebook.net/en_US/sdk.js","facebook-jssdk");
        //   (function(d, s, id) {
        //     var js, fjs = d.getElementsByTagName(s)[0];
        //     if (d.getElementById(id)) return;
        //     js = d.createElement(s); js.id = id;
        //     js.src = "//connect.facebook.net/en_US/sdk.js";
        //     fjs.parentNode.insertBefore(js, fjs);
        // }(document, 'script', 'facebook-jssdk'));
        console.log(document);
  }

  componentDidMount() {
        this.loadFbLoginApi(this);
    }

    testAPI = () => {
      console.log('Welcome!  Fetching your information.... ');
      (window as any).FB.api('/me', function(response) {
      console.log(response);
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
      });
    }

    statusChangeCallback = (response) => {
      console.log('statusChangeCallback');
      console.log(response);
      if (response.status === 'connected') {
        this.testAPI();
      } else if (response.status === 'not_authorized') {
          console.log("Please log into this app.");
      } else {
          console.log("Please log into this facebook.");
      }
    }

    checkLoginState = () => { 

      this.state.fb.getLoginStatus(function(response) {
        this.statusChangeCallback(response);
      }.bind(this));
    }

    handleFBLogin = () => {
      console.log("handle",this.state.fb);
      this.state.fb.login(this.checkLoginState());
        }

    render() {
        return (
                <div>
                    <Button className={`loginBtn loginBtn--facebook ${this.props.className}`}
                    onClick = {this.handleFBLogin} size="medium" variant="contained">
                    Sign in with Facebook
                    </Button>
                </div>
               );
    }
}