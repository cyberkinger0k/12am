import React, { Component } from "react";

export interface IProps {
    children?: React.ReactNode;
    content?: string;
    className?: string;
    id?: string;
}
export class A extends Component<IProps, {}> {

    public render() {

        return (
           <div id={this.props.id} className={`wms-ui-a'${this.props.className}`}>
               {this.props.content}
           </div>
        );
    }
}
