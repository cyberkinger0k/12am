import Tooltip from "@material-ui/core/Tooltip";
import React, {Component} from "react";
import { withStyles } from "@material-ui/core";

export interface ITooltipProps {
    children?: React.ReactElement;
    id?: string;
    title: any;
    arrow?: boolean;
    type?:"light";
    placement?:'bottom-end'| 'bottom-start'| 'bottom'| 'left-end'| 'left-start'| 'left'| 'right-end'| 'right-start'| 'right'| 'top-end'| 'top-start'| 'top'
}
const LightTooltip = withStyles((theme: any) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: theme.shadows[1],
      fontSize: 11,
    },
  }))(Tooltip);
export class MToolTip extends Component<ITooltipProps, {}> {

    constructor(props: ITooltipProps) {
        super(props);
    }
    
    public render() {
        const props = this.props;
        if(this.props.type === "light"){
        return (
            <LightTooltip
            id={props.id}
            title={props.title}
            placement={props.placement}
            >
            {props.children}
            </LightTooltip>
        );
        }else{
            return (
                <Tooltip
                 id={props.id}
                 title={props.title}
                 placement={props.placement}
                >
                {props.children}
                </Tooltip>
            );
        }

    }
}
