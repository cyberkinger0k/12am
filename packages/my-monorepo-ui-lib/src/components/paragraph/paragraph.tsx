import React , { Component } from "react";

export interface paragraphProps {
    children?: React.ReactNode;
    className?: any;
    id?: any;
}
export class P extends Component<paragraphProps, {}> {

    public render() {

        return (
           <p id={`wms-ui-paragraph ${this.props.id}`} className={`wms-ui-paragraph ${this.props.className}`}>
                {this.props.children}
           </p>
        );
    }
}
