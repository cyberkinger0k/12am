import React, { Component } from 'react';
import "./ProgressBar.scss";

export interface ProgressBarProps {
    width:string;
    meterclassName?:string;
    progressclassName?:string;
    showvalue?:boolean
    type?:"valuebar";
    valuebarcointainer?:string;
}
export class ProgressBar extends Component< ProgressBarProps, {}> {
    public render() {
        if(this.props.type === "valuebar"){
        return(
            <div className={`valuebar-cointainer ${this.props.valuebarcointainer}`}>
            <div className={`meter valuebar ${this.props.meterclassName}`}>
               <span className={"barcointainer"} style={{width: this.props.width}}>
               <span className={`progress barcointainer ${this.props.progressclassName}`}>
               </span>
               </span>
            </div>
            <span className="progress-value barcointainer">{this.props.width}</span>
            </div>
        );
        }else{
        return(
            <div className={`meter ${this.props.meterclassName}`}>
               <span className={"barcointainer"} style={{width: this.props.width}}>
               <span id="mybar" className={`progress barcointainer ${this.props.progressclassName}`}>
               </span>
               </span>
            </div>
        );
        }
    }
}
