import React from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import './ModalComponent.scss';

export interface ModalComponentProps {
    children?: any;
    className?: string;
    id?:string;
    open:boolean;
    handleClose?:any;
    header?:string;
    content?:string;
    // onClick?: (e:any) => void
}

export const ModalComponent : React.FC<ModalComponentProps> = (props:ModalComponentProps) =>{
  return (
    <div className={`wms-ui-modalComponent ${props.className}`}>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={props.open}
          onClose={props.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <div  className='paper'>
            <h2 id="transition-modal-title">{props.header}</h2>
            <div id="transition-modal-description">{props.children}</div>
          </div>
        </Modal>
      </div>
    );
  
}

