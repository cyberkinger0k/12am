import React, { Component } from 'react';
import { Tab } from '@material-ui/core';
import "./TabComponent.scss";

export interface ITabProps {
    onClick?: () => any;
    id?: any;
    className?: any;
    label: string;
    value: string;
}

// const styles = theme => ({
//     root: {
//       flexGrow: 1,
//       backgroundColor: theme.palette.background.paper
//     },
//     label: {
//       color: "#FFF000"
//     },
//     indicator: {
//       backgroundColor: "#FFF"
//     }
//   });
export class TabComponent extends Component<ITabProps, {}> {

    public render() {
       ;
        return(
                <Tab
                className={this.props.className}
                value={this.props.value}
                label={this.props.label}
                onClick={this.props.onClick}
                textColor="primary"
                >
                </Tab>
        );
    }
}
