import React, { Component } from 'react';
import { AppBar, Tab } from '@material-ui/core';
import Tabs from '@material-ui/core/Tabs';
import "./TabComponent.scss";

export interface ITabsProps {
    onChange?: (value?: any, name?: any) => any;
    id?: any;
    className?: any;
    variant: 'standard'| 'scrollable'| 'fullWidth';
    orientation: 'horizontal'| 'vertical';
    value: string;
}

const styles = theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper
    },
    label: {
      color: "#FFF000"
    },
    indicator: {
      backgroundColor: "#FFF"
    }
  });
export class TabsComponent extends Component<ITabsProps, {}> {

    public render() {
        const props = this.props;
        return(
            <AppBar position="static" color="default" className={this.props.className}>

                {/* <div style={styles}> */}
                <Tabs
                value={this.props.value}
                // onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant={this.props.variant}
                >
                {this.props.children}
                </Tabs>
                {/* </div> */}
            </AppBar>
        );
    }
}
