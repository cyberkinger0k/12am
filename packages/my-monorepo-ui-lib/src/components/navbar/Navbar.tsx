import React from "react";
import './NavbarStyles.scss';
import { Image } from '../image/Image';

export interface NavbarProps {
    css?: string;
    lists?: any;
    src?: string;
    altName?: string;
    onClick?: () => void;
}
export const Navbar: React.FC<NavbarProps> = (props: React.PropsWithChildren<NavbarProps>) => {
    const {lists, src, css, altName, onClick} = props;
    // tslint:disable-next-line: jsx-wrap-multiline
    return <header className="wms-header">
        {src ? <Image src={src} className={css} altName={altName}/> : null}
       
       <nav>
            <ul>
                {/*// tslint:disable-next-line: jsx-no-multiline-js*/}
                {lists.map((item: any, i: number) => {
                return (
                    <li key={i} className="wms-header-li">
                       {item.url ? <a href={item.url}> {item.name}</a> : 
                       <a onClick={onClick} className="wms-header-a"> {item.name}</a>} 
                    </li>
                    );
                 })
                }

                {/* <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Pricing</a></li>
                <li><a href="#">Terms of use</a></li>
                <li><a href="#">Contact</a></li> */}

            </ul>
        </nav>
  </header>;
};
