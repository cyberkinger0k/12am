import { Breadcrumbs as MBreadcrumbs, Link} from "@material-ui/core";
import React from "react";

export interface breadCrumbsProps {
   children?: React.ReactNode;
   className?: string;
   id?: string;
   separator?: React.ReactNode;
   data?:any[]
   onClick?:(data:any)=>any;
}

export class Breadcrumbs extends React.Component<breadCrumbsProps, {}> {
    onClick = (e) => {
       this.props.onClick && this.props.onClick(e.target.innerText);
       console.log("bread",e.target.innerText);
    }
    public render() {
        
        const props = this.props;
        return(
          <MBreadcrumbs className={`wms-ui-breadcrumbs ${props.className}`} id="wms-ui-breadcrumbs" separator={props.separator}>
             
              {props.data && props.data.map((data:string, index:number) => {
                   return <Link color="inherit" onClick={this.onClick}>{data}</Link>
                })
                }
              {props.children}
          </MBreadcrumbs>
        );
    }
}
