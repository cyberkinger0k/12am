import React, { Component } from 'react';
import {Card} from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import './Card.scss';

export interface CardProps {
    children?: React.ReactNode;
    className?: string;
    id?: string;
}

export class Cards extends Component<CardProps, {}> {

    public render() {
        return(
            <div>
                <Card className={`wms-ui-card ${this.props.className}`} id={`wms-ui-card ${this.props.id}`} >
                    <CardContent>
                        {this.props.children}
                    </CardContent>  
                </Card>
            </div>
        );
    }
}
