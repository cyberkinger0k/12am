import {CircularProgress as MCircularProgress} from "@material-ui/core";
import React from "react";
import "./spinner.scss";

export interface ProgressProps {
    className?:string;
    color?: any;
    variant?: "determinate" | "static";
}
export const Spinner: React.FC<ProgressProps> = (props: React.PropsWithChildren<ProgressProps>) => {
    const {color,variant,className} = props;
    return  <MCircularProgress
                className={`spinner ${className}`}
                color={color}
                variant={variant}
            />;
};
