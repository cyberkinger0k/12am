import {Snackbar as MSnackbar} from "@material-ui/core";
import React, { ReactNode } from "react";
import './SnackbarStyles.scss';

export interface SnackbarProps {
    open?: any;
    autoHideDuration?: number;
    message?: string;
    vertical?: "top" | "bottom" | undefined;
    horizontal?: "left" | "center" | "right" | undefined;
    position?: any;
    css?: string;
    action?: any;
}
export const Snackbar: React.FC<SnackbarProps> = (props: React.PropsWithChildren<SnackbarProps>) => {
    const {open, autoHideDuration, message, position, css, action} = props;
    return  <MSnackbar
    className={css}
    anchorOrigin={position}

    open={open}

    autoHideDuration={autoHideDuration}

    ContentProps={{
      "aria-describedby": "message-id",
    }}
    action={[action]}

    message={<span id="message-id">{message}</span>}

    />;
};
