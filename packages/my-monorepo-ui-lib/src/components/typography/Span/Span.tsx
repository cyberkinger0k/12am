import React, { Component } from "react";
export interface SpanProps {
    children?: React.ReactNode;
    className?: string;
    content?: string;
}
export class Span extends Component <SpanProps, {}> {
    public render() {
    return( <span className={this.props.className}>{this.props.content} Stories</span>
    );
}
}
