import Typography from "@material-ui/core/Typography";
import React, { Component } from "react";

export interface TypographyComponentProps {
    children?: React.ReactNode;
    content?: string;
    style?: any;
    className?: any;
    id?: any;
    variant?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "subtitle1" | "subtitle2" | "body1" | "body2" | "caption" | "button" | "overline" | "srOnly" | "inherit";
    color?: "initial" | "inherit" | "primary" | "secondary"
    | "textPrimary" | "textSecondary";
    align?: "inherit"| "left"| "center"| "right"| "justify";
}
export class TypographyComponent extends Component<TypographyComponentProps, {}> {

    public render() {

        return (
            <Typography
            className={this.props.className}
            variant={this.props.variant}
            gutterBottom={true}
            color={this.props.color}
            align={this.props.align}>
            {this.props.content}
            {this.props.children}
          </Typography>
        );
    }
}
