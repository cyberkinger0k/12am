import React from "react";
import { LineChart, BarChart } from "recharts";
import { CartesianGrid,Bar, XAxis, YAxis, Line, Tooltip, Legend } from 'recharts';
import { Margin } from '../../models/Margin';

export interface ChartProps{
    className:string,
    id:string,
    type:"line"|"bar",
    height?:number,
    width?:number,
    margin?:Margin,
    data:any,
    xKey: any;
    response: any;
    name: string;
    tooltipClassName?: string;
    tooltipWrapperClass?:string;
    barcolor?:any;
}

const getQuestion = (label) => {
    if (label === 'Q1') {
      return "The training met my expectations";
    } if (label === 'Q2') {
      return "I will be able to apply the knowledge learned";
    } if (label === 'Q3') {
      return "The training objectives for each topic were identified and followed";
    } if (label === 'Q4') {
      return 'The content was organized and easy to follow';
    } if (label === 'Q5') {
      return 'The materials distributed were pertinent and useful';
    } if (label === 'Q6') {
      return 'The trainer was knowledgeable';
    } if (label === 'Q7') {
        return 'The quality of instruction was good';
    } if (label === 'Q8') {
        return 'The trainer met the training objectives';
    } if (label === 'Q9') {
        return 'Class participation and interaction were encouraged';
    } if (label === 'Q10') {
        return 'Adequate time was provided for questions and discussion';
    }
  };
  interface tooltipProps {
      active?: any;
      label?: any;
      name: any;
      tooltipclass?: string;
      tooltipWrapperClass?: string;
  }
  const CustomTooltip: React.FC<tooltipProps> = ({active, label, name, tooltipclass,tooltipWrapperClass}) => {
    if (active && name === "Feedbacks") {
      return (
        <div className={`custom-tooltip ${tooltipclass}`}>
          <p className={`intro ${tooltipclass}`}>{label && label}:{getQuestion(label && label)}</p>
        </div>
      );
    }
  
    return null;
  };
 
  
export class Chart extends React.Component<ChartProps,{}>{

    render(){

        const props = this.props;
        const xKey = props.xKey;
        const chartData = props.data;
        const response = props.response;
        let data=[];    
        let rating=[0,0,0,0,0];
        let result:number[] =[0,0,0,0,0,0,0,0,0,0] ;  
        let length = 0;
        
      
      if ( props.name == "Overall Review"){
        
        for (let i = 0;i < response.length; i++){
            let obj = {
                response_value : response[i].value,
                rating : xKey[i]
            }
           data.push(obj); 
        }
      }

    else if ( props.name == "Feedbacks"){
      
     if (chartData.length !== 0){
      for (let i = 0; i < 5; i++){
          length = length + chartData[0].rating[i];
      }

        for( let i = 0 ; i < 10; i++){
          
           for(let j = 0; j < 5; j++)
            {
               result[i] = result[i] + chartData[i].rating[j] * (5-j);
            }
        }
 
      for(let j = 0; j < response.length; j++)
      {
         result[j] = Math.round(result[j]/length);
      }

      for (let i = 0;i < response.length; i++){
          let obj = {
              [xKey] : response[i],
              rating  : result[i]
          }
         data.push(obj); 
      } 
    }
    }
    if(props.type === 'line'){
            return(    
       
                <LineChart className = {props.className} width={props.width} height={props.height} data={data} margin={props.margin}>
                    <XAxis dataKey={xKey}/>
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Line type="monotone" dataKey="rating" stroke={this.props.barcolor} activeDot={{ r: 8 }} />
                </LineChart>
            );
        }
    else{       
        return(
            <BarChart className = {props.className} width={props.width} height={props.height} data={data} margin={props.margin}>
             <XAxis dataKey={"response_value"} />
                <YAxis  type="number" domain={[0, 5]}/> 
                 <Tooltip content = {props.name === "Feedbacks" ?<CustomTooltip name={props.name} tooltipWrapperClass={props.tooltipWrapperClass} tooltipclass={props.tooltipClassName}/> : undefined} />
                <Legend />
                <CartesianGrid strokeDasharray="3 3"/>
                <Bar dataKey="rating" fill={this.props.barcolor} />
            </BarChart>           
        );
    }
 }

}