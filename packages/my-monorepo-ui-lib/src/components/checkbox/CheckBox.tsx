import Checkbox from "@material-ui/core/Checkbox";
import React, {Component} from "react";

export interface ICheckboxProps {
    onChange?: (event?: object) => any;
    id?: string;
    className: string;
    checked?: boolean;
    color?: "primary"| "secondary"| "default";
    value?: any;
    disabled?: boolean;
}

export class CheckBox extends Component<ICheckboxProps, {}> {

    public render() {
        const props = this.props;
        return (
            <Checkbox
             className={props.className}
             id={props.id}
             onChange={props.onChange}
             checked={props.checked}
             color={props.color}
             disabled={props.disabled}
             value={props.value}
            />
        );
    }
}
