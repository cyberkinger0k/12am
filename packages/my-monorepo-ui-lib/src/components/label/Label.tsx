import React , { Component } from "react";
import './Label.scss';

export interface LabelProps {
    children?: React.ReactNode;
    content?: string;
    id?: string;
    className?: string;
    required?: boolean;
}
export class Label extends Component<LabelProps, {}> {

    public render() {

        return (
           <label id={this.props.id} className={`wms-ui-lable ${this.props.className}`}>
               {this.props.content}
               {this.props.required ? <span className='red-color'>*</span> : null}
               
           </label>
        );
    }
}
