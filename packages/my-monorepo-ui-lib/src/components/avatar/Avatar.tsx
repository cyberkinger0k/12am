import React from "react";
import { Avatar as MAvatar} from "@material-ui/core";
import "./Avatar.scss";

export interface IAvatarProps {
   children?: React.ReactNode;
   className?: string;
   id?: string;
   src?: string;
   alt?: string;
}

export class Avatar extends React.Component<IAvatarProps, {}> {

public render() {
    const props = this.props;
    return(
        <MAvatar className={`wms-ui-avatar ${props.className}`} id={props.id}
                src={props.src} alt={props.alt}> {props.children}</MAvatar>

        );
}

}
