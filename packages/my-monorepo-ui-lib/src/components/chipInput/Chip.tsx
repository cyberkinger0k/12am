import React, { Component } from 'react';
import ChipInput from 'material-ui-chip-input'


export interface TagsProps {
    handleAddChips?: (chip: any) => any;
    handleDeleteChips?: (chip: any, index: number) => any;
    onChange?: (value?: any, name?: any) => any;
    value?: any;
    className?: string;
    name?: string;
    dataSource?: any;
    chipRenderer?: (any) => any;
    variant?: "outlined" | "standard" | "filled";
    placeholder?:string;
}
export class Chip extends Component<TagsProps, {}>{


    handleAddChip = (chip: any) => {
        console.log("chip", chip);
        this.props.handleAddChips && this.props.handleAddChips(chip);
    }
    handleDeleteChip = (chip: any, index: number) => {
        console.log("chip", chip);
        this.props.handleDeleteChips && this.props.handleDeleteChips(chip, index);
    }
    handleChange = (e: any) => {
        this.props.onChange && this.props.onChange(e.target.value, e.target.name);
    }

    render() {
        return (

            <ChipInput
                value={this.props.value}
                onAdd={this.handleAddChip}
                onDelete={this.handleDeleteChip}
                dataSource={this.props.dataSource}
                chipRenderer={this.props.chipRenderer}
                placeholder={this.props.placeholder}
            />

        )
    }
}

