import React , { Component } from "react";
import "./CommentTable.scss";

export interface CommentTableProps {
    children?: React.ReactNode;
    id?: string;
    className?: string;
}
export class CommentTable extends Component<CommentTableProps, {}> {

    public render() {

        return (
            <table className={`main-table ${this.props.className}`}>
                <tbody>
                {this.props.children}
                </tbody>
            </table>
        );
    }
}