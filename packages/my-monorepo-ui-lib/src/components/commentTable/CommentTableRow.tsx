import React , { Component } from "react";
import "./CommentTable.scss";

export interface CommentTableRowProps {
    children?: React.ReactNode;
    id?: string;
    className?: string;
    name?: string;
    comment?: string;
    datetime?: string;
    commentType?: string;
}
export class CommentTableRow extends Component<CommentTableRowProps, {}> {

    public render() {

        return (
                <tr>
                    <td className="name">
                        {this.props.name}
                    </td>
                    <td className="comment">
                            {this.props.comment}
                        <div className="date">
                            {this.props.datetime}
                        </div>
                        <div className="type">
                            {this.props.commentType}
                        </div>
                    </td>
                </tr>
        );
    }
}