import React from "react";
import "./ButtonLoader.scss";

export interface IButtonLoaderProps {
   type: string;
   children?: React.ReactNode;
   className?: string;
   id?: string;
}

export class ButtonLoader extends React.Component<IButtonLoaderProps, {}> {

public render(){
    return(
        <div className={this.props.className}>
            {this.props.type==="circular"?
            <span className="spinner-border spinner-grow-sm" role="status" aria-hidden="true"></span>:
            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>}
            <span className="sr-only">{this.props.children}</span>
        </div>
        );
}

}