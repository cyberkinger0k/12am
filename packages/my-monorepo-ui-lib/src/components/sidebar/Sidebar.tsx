import React from "react";
import Collapse from "@material-ui/core/Collapse";
import { MdExpandMore, MdExpandLess} from 'react-icons/md';
import {Link, BrowserRouter} from '../../../../../node_modules/react-router-dom';
import './Sidebar.scss';

function SidebarItem({ item }) {
    const [collapsed, setCollapsed] = React.useState(true);
    const { label, path, icon, onClick: onClickProp, children } = item;
  
    function toggleCollapse() {
      setCollapsed(prevValue => !prevValue);
    }
  
    function onClick(e) {
      if (Array.isArray(children)) {
        toggleCollapse();
      }
      if (onClickProp) {
        onClickProp(e, item);
      }
    }
  
    let expandIcon;
  
    if (Array.isArray(children) && children.length) {
      expandIcon = !collapsed ? (
        <MdExpandLess
          className={
            "sidebar-item-expand-arrow" + " sidebar-item-expand-arrow-expanded"
          }
        />
      ) : (
        <MdExpandMore className="sidebar-item-expand-arrow" />
      );
    }
  
    return (
      <>
        <li className="sidebar-li" onClick={onClick}>
            <BrowserRouter>
            <Link to={path} className={`wms-ui-sidebar-a`}>
            <span className="sidebar-span">
                <div className="sidebar-icon-div">
                    {icon && icon}
                </div>
                    {label}
                
                </span>
                {expandIcon}
            </Link>
            </BrowserRouter>
        </li>
        <Collapse in={!collapsed} timeout="auto" unmountOnExit>
          {Array.isArray(children) ? (
            <ul className="sidebar-ul">
              {children.map((subItem, index) => (
                <React.Fragment key={`${subItem.name}${index}`}>
                    <SidebarItem
                      item={subItem}
                    />
                </React.Fragment>
              ))}
            </ul>
          ) : null}
        </Collapse>
      </>
    );
  }

  export interface ISidebarProps {
    routes?: any;
    className?: string;
    aclassName?: string;
}
export const Sidebar: React.FC<ISidebarProps> = (props: React.PropsWithChildren<ISidebarProps>) => {
  const {routes, className, children} = props;
  return (<div className={`wms-ui-maincontainer`}>
            <div className={`wms-ui-sidebar-container ${className}`}>
                <div className="sidebar-inner-container">
                <ul className="sidebar-ul">
                    {routes.map((sidebarItem, index) => (
                    <React.Fragment key={`${sidebarItem.name}${index}`}>
                        <SidebarItem
                            item={sidebarItem}
                        />
                    </React.Fragment>
                    ))}
                </ul>
                </div>
            </div>
            
            <div className={`wms-ui-container`}>
                {children}
            </div>
            </div>
        );
};
