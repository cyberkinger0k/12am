import React, { Component } from 'react';
import {Select as MSelect} from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import './SelectBox.scss';

export interface selectProps {
    onChange?: (value?: any, name?: any) => any;
    id?: any;
    className?: any;
    class?: string;
    name?: string;
    value?: any;
    label?: any;
    disabled?: boolean;
    placeholder?: any;
}

export class SelectBox extends Component<selectProps, {}> {

    public handleChange = (e: any) => {
        this.props.onChange && this.props.onChange(e.target.value, e.target.name);
     }
    public handleClick = () => {
        alert("hello");
    }
    public render() {
        const props = this.props;
        return(
            <FormControl className="select-Box">
            <InputLabel>{props.name}</InputLabel>
            <MSelect onChange={this.handleChange} displayEmpty={true} name={props.name} className={`wms-ui-select ${props.className}`}
                     id={`wms-ui-select ${props.id}`} disabled={props.disabled}>
                    <MenuItem disabled={true} selected={true}>{props.placeholder}</MenuItem>
                         {props.value.map((options: any) =>
                        <MenuItem value={options.id} onClick={this.handleClick}>{options.value}</MenuItem>,
                    )}
        </MSelect>
        </FormControl>
        );
    }
}
