import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import './Datagrid.scss';
import './ag-grid.css';
import './ag-theme-balham.css';
import { Column } from '../../models';

let gridApi:any;
export interface DataGridProps {
  rows?: any;
  columns?: Column[];
  id?: string;
  className?: string;
  onRowSelect?: any;
  loadingTemplate?:any;
  noRowsTemplate?:any;
  rowDrag?:boolean;
  pagesize?:number;
  multirowselect?:boolean;

}
export class DataGrid extends Component<DataGridProps, {}> {

  localeText: {
    noRowsToShow: "No records to show"
  }

 private onGridReady = (gridapi:any) => {
  gridApi = gridapi.api;
  gridApi.sizeColumnsToFit();

}

private onSelectionChanged = () => {
  const _selectedRows = gridApi.getSelectedRows();
  this.props.onRowSelect && this.props.onRowSelect(_selectedRows);
}
  render() {

    
    return (
      <div className={`wms-ui-dataGrid ag-theme-balham ${this.props.className}`}>
        <AgGridReact
          columnDefs={this.props.columns}
          rowData={this.props.rows}
          rowSelection='single'
          rowMultiSelectWithClick={this.props.multirowselect}
          onSelectionChanged={this.onSelectionChanged}
          onGridReady={this.onGridReady}
          overlayLoadingTemplate={this.props.loadingTemplate? this.props.loadingTemplate:"loading...."}
          overlayNoRowsTemplate={this.props.noRowsTemplate}
          rowHeight={40}
          headerHeight={40}
          localeText={this.localeText}
          rowDragManaged={this.props.rowDrag}
          rowModelType={this.props.pagesize?"pagination":undefined}
          pagination={this.props.pagesize? true:false}
          paginationPageSize={this.props.pagesize}
          
         >
        </AgGridReact>
      </div>
    );
  }
}
