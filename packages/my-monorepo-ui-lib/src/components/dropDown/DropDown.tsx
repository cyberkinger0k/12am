import React, { Component } from "react";
import { Button } from "../index";
import "./DropDown.scss";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { FaCaretDown } from "react-icons/fa";

export interface IDropDownProps {
    value: any;
    label: string;
    className?: string;
    listClassName?: string;
    id?: string;
    disabled?: boolean;
    classbtn?:string;
    type?: "icon";
}
export class DropDown extends Component <IDropDownProps, {}>{
    // dropdownContainer = React.createRef<HTMLDivElement>();
    state = {
        open:false,
    }
    
    // public componentDidMount() {
    //   window.onmousedown = (ev: MouseEvent): any => {
    //        this.handleClose(ev)
    //   }
    // }
    // toggleDropdown(event) {
    //   console.log(event);
    //   if(event.target){
    //   this.setState({ open: false });
    //   }
    // }
    
    public render() {
      const props = this.props; 
        return (
          <div className={`wms-ui-Dropdown  ${props.className}`}
          onBlur={this.handleBlur}>
            <div className="dropdownContainer">
              {props.type === "icon" ? <div tabIndex={0} onClick={this.handleClick} className={`wms-ui-dropdown-button ${props.classbtn}`} ><MoreVertIcon /></div>:
              <Button variant="outlined" className={`wms-ui-dropdown-button ${props.classbtn}`} onClick={this.handleClick}>{props.label}<FaCaretDown/>
              </Button>}
              {props.value ?
              <span>
              {this.state.open && (
                <div className={`container listcontent ${props.listClassName}`}>
                  <ul className="wms-ui-ul">
                       {props.value.map((options: any,index:number) =>
                       {return options.value?
                       <li className="wms-ui-li" onClick={()=>this.click(options)} value={options.id} key={index}>{options.value}</li>:null}
                    )}
                  </ul>
                </div>
              )}
              </span>
              :undefined}
            </div>
          </div>
        );
      }
private click = (options:any) => {
  options.onclick();
  this.handleClick();
}
 private handleClick = () => {
   this.setState({open: !this.state.open});  
 };
//  private handleClose = (e) => {
//   if (this.dropdownContainer.current && !this.dropdownContainer.current.contains(e.target)){
//     this.setState({open: false});
//   }
//   }
 private handleBlur = (e:any) => {
    // firefox onBlur issue workaround
    if (e.nativeEvent.explicitOriginalTarget &&
        e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) {
          return;
    }

    if (this.state.open) {
      setTimeout(() => {
        this.setState({ open: false });
      }, 200);
    }
}
}