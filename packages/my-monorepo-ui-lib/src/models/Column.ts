export interface Column {
    headerName: string;
    field: string;
    sortable: boolean;
    filter: boolean;
}
