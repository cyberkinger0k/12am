import React from 'react';
import * as ts from 'react-test-renderer';
import { ProgressBar } from '../src/components';

describe("Process bar components", () => {

     test("Snapshot testing", () => {
          const progressbar = ts.create(<ProgressBar progressclassName={"bg-info progress-bar-striped"} width={"25%"}
          type="valuebar"/>)
           .toJSON();

           expect(progressbar).toMatchSnapshot();
     });
     
     test('progressbar Component should render without error', () => {
          const testRenderer = ts.create(<ProgressBar progressclassName={"bg-info progress-bar-striped"} width={"25%"}
          type="valuebar"/>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(ProgressBar).props.width).toBe('25%');
          expect(testInstance.findByType(ProgressBar).props.type).toBe('valuebar');
          expect(testInstance.findByType(ProgressBar).props.progressclassName).toBe("bg-info progress-bar-striped");
        });

});
