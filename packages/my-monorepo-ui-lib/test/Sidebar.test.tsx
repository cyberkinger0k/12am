import React from 'react';
import * as ts from 'react-test-renderer';
import { Sidebar }  from '../src/components';

const nav = [
    { path: '/', title: 'Dashboard' },
    { path: '/mail/inbox', title: 'Inbox' },
    { path: '/mail/sent', title: 'Sent'},
    { path: '/mail/drafts', title: 'Drafts'},
    { path: '/account', title: 'Profile'}
];

describe('Sidebar Component', () => {
  it('Snapshot testing', () => {
    const tree = ts.create(<Sidebar routes={nav}></Sidebar>)
      .toJSON();
    expect(tree).toMatchSnapshot();
    
  });
});
