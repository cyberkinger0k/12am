import React from 'react';
import * as ts from 'react-test-renderer';
import { MToolTip, Button } from '../src/components';

describe("Tooltip components", () => {

     test("Snapshot testing", () => {
          const tooltip = ts.create(<MToolTip title="tooltip" id="tooltip-1">
          <span><Button variant="contained" btnColor="primary">Button</Button></span>
        </MToolTip>)
           .toJSON();

           expect(tooltip).toMatchSnapshot();
     });

     test('Component should render without error', () => {
          const testRenderer = ts.create(<MToolTip title="tooltip" id="tooltip-1">
          <span><Button variant="contained" btnColor="primary">Button</Button></span>
        </MToolTip>);
          const testInstance = testRenderer.root;
          expect(testInstance.findByType(MToolTip).props.id).toBe('tooltip-1');
          expect(testInstance.children.length).toBe(1);
          expect(testInstance.findByType(MToolTip).props.title).toMatch('tooltip');

        });

});
