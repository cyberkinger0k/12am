import React from 'react';
import * as ts from 'react-test-renderer';
import { Breadcrumbs } from '../src/components';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

describe("Avatar components", () => {

    test("Breadcrumbs Snapshot testing", () => {
         const avatar = ts.create(<Breadcrumbs className="breadcrumbs-1">
                  <Link color="inherit" href="/" >Material-UI </Link>
                  <Link color="inherit" href="/" >Core</Link>
                  <Typography color="textPrimary">Breadcrumb</Typography>
            </Breadcrumbs>)
          .toJSON();
          expect(avatar).toMatchSnapshot();
    });
    
    test('Breadcrumbs Component should render without error', () => {
         const testRenderer = ts.create(<Breadcrumbs className="breadcrumbs-1">
                  <Link color="inherit" href="/" >Material-UI </Link>
                  <Link color="inherit" href="/" >Core</Link>
                  <Typography color="textPrimary">Breadcrumb</Typography>
            </Breadcrumbs>);
         const testInstance = testRenderer.root; 
         expect(testInstance.findByType(Breadcrumbs).props.className).toBe('breadcrumbs-1');
       });

});

