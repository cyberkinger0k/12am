import React from 'react';
import * as ts from 'react-test-renderer';
import { SelectBox } from '../src/components';

const list = [
    {   id:1,
        value:'one'
    },
    {
        id:2,
        value:'two'
    },
    {
        id:3,
        value:'Three'
    }
]

describe("SelectBox components", () => {

     test("Snapshot testing", () => {
          const select = ts.create( <SelectBox className="select" id="select" value={list} name="Options"/>)
           .toJSON();

           expect(select).toMatchSnapshot();
     });
     
     test('Selectbox Component should render without error', () => {
          const testRenderer = ts.create(<SelectBox className="select" id="select" value={list} name="Options"/>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(SelectBox).props.id).toBe('select');
          expect(testInstance.findByType(SelectBox).props.className).toBe('select');
          expect(testInstance.findByType(SelectBox).props.placeholder).toBe(undefined);
        });

});
