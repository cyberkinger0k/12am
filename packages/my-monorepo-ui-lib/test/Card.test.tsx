import React from 'react';
import * as ts from 'react-test-renderer';
import { Cards, Button } from '../src/components';

describe("Card components ", () => {

     test("Snapshot testing", () => {
          const paragraph = ts.create(<Cards id="card" className="card">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B" 
                    className="card-img-top" />
                    <div className="card-body">
                    <h4 className="card-title">Lizard</h4>     
                    <p className="card-text"> Lizards are a widespread group of squamate reptiles, with over 6,000 species,
                    ranging across all continents except Antarctica</p>
                    <Button size="small" btnColor="primary">
                    Share
                    </Button>
                    <Button size="small" btnColor="primary">
                    Learn More
                    </Button>
                    </div>
                </Cards>).toJSON();

           expect(paragraph).toMatchSnapshot();
     });
     
     const testRenderer = ts.create(<Cards id="card" className="card">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B" 
        className="card-img-top" />
        <div className="card-body">
        <h4 className="card-title">Lizard</h4>     
        <p className="card-text"> Lizards are a widespread group of squamate reptiles, with over 6,000 species,
        ranging across all continents except Antarctica</p>
        <Button size="small" btnColor="primary">
        Share
        </Button>
        <Button size="small" btnColor="primary">
        Learn More
        </Button>
        </div>
    </Cards>);
     const testInstance = testRenderer.root;

     test("should render without error", () => {
          
          expect(testInstance.findByType(Cards).props.id).toBe('card');
          expect(testInstance.findByType(Cards).props.className).toMatch('card');
        });

     test("should contain one children", ()=>{
          expect(testInstance.children.length).toBeGreaterThanOrEqual(1);
     });   

})