import React from 'react';
import * as ts from 'react-test-renderer';
import { Chart } from "../src/components/chart";

const chartData = [
  {
    id:1,
    user : {
      id:101,
      name:'anil'
    },
    session : {
      id : 1001,
      name : "Git Essential Training" 
    },
    response_value : 5,
    answers : [1,2,3,4,5,5,4,3,2,5]

  },
  {
    id:2,
    user : {
      id:102,
      name:'Tarni'
    },
    session : {
      id : 1001,
      name : "Git Essential Training" 
    },
    response_value : 5,
    answers : [1,2,3,4,5,5,4,3,2,5]
  },
  {
    id:3,
    user : {
      id:103,
      name:'Vikram'
    },
    session : {
      id : 1001,
      name : "Git Essential Training" 
    },
    response_value : 2,
    answers : [4,2,3,4,3,5,4,3,2,5]
  },
  {
    id:4,
    user : {
      id:104,
      name:'Kashif'
    },
    session : {
      id : 1001,
      name : "Git Essential Training" 
    },
    response_value : 3,
    answers : [1,5,3,4,2,1,4,3,2,1]
  },
  {
    id:5,
    user : {
      id:105,
      name:'Devesh'
    },
    session : {
      id : 1001,
      name : "Git Essential Training" 
    },
    response_value : 4,
    answers : [2,3,3,4,2,1,4,3,2,1]
  },

];

const response = [
{key:1,value:'Very Poor'},
{key:2,value:'Poor'},
{key:3,value:'Average'},
{key:4,value:'Good'},
{key:5,value:'Excellent'}
];
  describe("Chart components", () => {

    test("Snapshot testing", () => {
         const charts = ts.create(<Chart type="bar" name="Overall Review" className="chart-2" id="chart-2" data={chartData} width={700} height={300}
         margin={{ top: 5, right: 30, left: 20, bottom: 5,}}  xKey={'response_value'}  response={response}>
             
     </Chart>).toJSON();

          expect(charts).toMatchSnapshot();
    });
    
    test('Chart Component should render without error', () => {
         const testRenderer = ts.create(<Chart type="bar" name="Overall Review" className="chart-2" id="chart-2" data={chartData} width={700} height={300}
         margin={{ top: 5, right: 30, left: 20, bottom: 5,}}  xKey={'response_value'}  response={response}>     
     </Chart>);
         const testInstance = testRenderer.root;
     
         expect(testInstance.findByType(Chart).props.type).toMatch('bar');
         expect(testInstance.findByType(Chart).props.className).toBe('chart-2');
       })
});
