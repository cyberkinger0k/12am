import React from "react";
import * as ts from "react-test-renderer";
import { GridView, Cards} from "../src/components";

describe("Grid components", () => {

     test("Snapshot testing", () => {
          const gridview = ts.create(<GridView  container direction="row-reverse" alignItems="baseline">
                            <GridView item xs={12} >
                            <Cards className="gridview-test" >xs=12</Cards>
                            </GridView>
                            <GridView item xs={12} md={6} >
                            <Cards className="gridview-test" >xs=12 md=6 col1</Cards>
                            </GridView>
                            <GridView item xs={12} md={6}>
                            <Cards className="gridview-test" >xs=12 md=6 col2</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                        </GridView>).toJSON();

           expect(gridview).toMatchSnapshot();
     });
     
     test('Grid Component should render without error', () => {
          const testRenderer = ts.create(<GridView  container direction="row-reverse" alignItems="baseline">
                            <GridView item xs={12} >
                            <Cards className="gridview-test" >xs=12</Cards>
                            </GridView>
                            <GridView item xs={12} md={6} >
                            <Cards className="gridview-test" >xs=12 md=6 col1</Cards>
                            </GridView>
                            <GridView item xs={12} md={6}>
                            <Cards className="gridview-test" >xs=12 md=6 col2</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                            <GridView item xs={6} sm={3}>
                            <Cards className="gridview-test" >xs=6 sm=3</Cards>
                            </GridView>
                        </GridView>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(GridView).props.container).toBe(true);
          expect(testInstance.findByType(GridView).props.className).toBe(undefined);
          expect(testInstance.findByType(GridView).props.item).toBe(undefined);
        });

});
