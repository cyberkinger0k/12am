import React from 'react';
import * as ts from 'react-test-renderer';
import { Spinner }  from '../src/components';

describe("Spinner", () => {

    test("Snapshot testing", () => {
        const spinner = ts.create(<Spinner color="secondary"></Spinner>)
        .toJSON();

          expect(spinner).toMatchSnapshot();
    });
    
    test('Spinner Component should render without error', () => {
         const testRenderer = ts.create(<Spinner color="secondary"></Spinner>);
         const testInstance = testRenderer.root;
     
         expect(testInstance.findByType(Spinner).props.color).toBe('secondary');
       });
});