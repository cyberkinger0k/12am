import React from 'react';
import * as ts from 'react-test-renderer';
import { InputField } from '../src/components';

describe("Input components", () => {

     test("Snapshot testing", () => {
          const input = ts.create(<InputField type="Number" name="Age"  value='23'  label="Age" className="age"/>)
           .toJSON();

           expect(input).toMatchSnapshot();
     });
     
     test('Input Component should render without error', () => {
          const testRenderer = ts.create(<InputField type="Number" name="Age"  value='23'  label="Age" className="age"/>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(InputField).props.type).toBe('Number');
          expect(testInstance.findByType(InputField).props.className).toBe('age');
          expect(testInstance.findByType(InputField).props.placeholder).toBe(undefined);
        });

});