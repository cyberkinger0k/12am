import React from 'react';
import * as ts from 'react-test-renderer';
import { Label }  from '../src/components';


test('Label snapshot', () => {
  const tree = ts.create(<Label content='STORY'></Label>)
    .toJSON();
  expect(tree).toMatchSnapshot();
})