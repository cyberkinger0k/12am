import React from 'react';
import * as ts from 'react-test-renderer';
import { Button }  from '../src/components';

describe('Button Component', () => {
  it('Snapshot testing', () => {
    const tree = ts.create(<Button variant="contained" btnColor="secondary">Hello</Button>)
      .toJSON();
    expect(tree).toMatchSnapshot();
    
    
  });

  it('Button should render correctly', () => {
    const testRenderer = ts.create(<Button variant="contained" btnColor="secondary">Hello</Button>);
    const testInstance = testRenderer.root;

    expect(testInstance.findByType(Button).props.variant).toBe('contained');
    
  });
});
// import { shallow } from 'enzyme';
// import { Button }  from '../src/components/buttons/Button';


// test('check snapshot', () => {
//   const checkbox = shallow(<Button size="medium" variant="contained" btnColor="default">Default</Button>);

//   // Snapshot demo
//   expect(checkbox).toMatchSnapshot();
// })
