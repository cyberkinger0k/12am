import React from 'react';
import * as ts from 'react-test-renderer';
import { DropDown } from '../src/components';

const list = [
    {   id: 1,
        value: "Option 1",
    },
    {
        id: 2,
        value: "Option 2",
    },
    {
        id: 3,
        value: "Option 3",
    },
    ,
    {
        id: 4,
        value: "Option 4",
    }
];

describe("DropDown components", () => {

     test("Snapshot testing", () => {
          const dropDown = ts.create( <DropDown className="dropdown" id="dropdown" value={list} label="DropDown" name="Options"/>)
           .toJSON();

           expect(dropDown).toMatchSnapshot();
     });
     
     test('DropDown Component should render without error', () => {
          const testRenderer = ts.create(<DropDown className="dropdown" id="dropdown" label="DropDown" value={list} name="Options"/>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(DropDown).props.id).toBe('dropdown');
          expect(testInstance.findByType(DropDown).props.className).toBe('dropdown');
          expect(testInstance.findByType(DropDown).props.placeholder).toBe(undefined);
        });

});
