import React from "react";
import * as ts from "react-test-renderer";
import { CheckBox } from "../src/components";

describe("Card components ", () => {

     test("Snapshot testing", () => {
          const checkbox = ts.create(<CheckBox className="check-box" color="default" checked={true}/>).toJSON();

           expect(checkbox).toMatchSnapshot();
     });

     const testRenderer = ts.create(<CheckBox className="check-box" color="default" checked={true}/>);
     const testInstance = testRenderer.root;

     test("should render without error", () => {

          expect(testInstance.findByType(CheckBox).props.color).toBe("default");
          expect(testInstance.findByType(CheckBox).props.className).toMatch("check-box");
          expect(testInstance.children.length).toBeGreaterThanOrEqual(0);
        });

});
