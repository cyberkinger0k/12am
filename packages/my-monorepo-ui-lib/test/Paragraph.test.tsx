
import React from 'react';
import * as ts from 'react-test-renderer';
import { P } from '../src/components';

describe("Input components ", () => {

     test("Snapshot testing", () => {
          const paragraph = ts.create(<P id="paragraph-1" className="paragraph-1">Hello, My name is Chris Hemsworth.</P>)
           .toJSON();

           expect(paragraph).toMatchSnapshot();
     });
     
     const testRenderer = ts.create(<P id="paragraph-1" className="paragraph-1">Hello, My name is Chris Hemsworth.</P>);
     const testInstance = testRenderer.root;

     test("should render without error", () => {
          
          expect(testInstance.findByType(P).props.id).toBe('paragraph-1');
        });

     test("should contain one children", ()=>{
          expect(testInstance.children.length).toBe(1);
     });   

})