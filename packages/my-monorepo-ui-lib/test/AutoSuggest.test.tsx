import React from 'react';
import * as ts from 'react-test-renderer';
import { AutoSuggest } from '../src/components';

const options = [
    { value: 'The Shawshank Redemption', year: 1994 },
    { value: 'The Godfather', year: 1972 },
    { value: 'The Godfather: Part II', year: 1974 },
    { value: 'The Dark Knight', year: 2008 },
    { value: '12 Angry Men', year: 1957 },
    { value: "Schindler's List", year: 1993 },
]
describe("Autosuggest components", () => {

    test("Snapshot testing", () => {
         const autoSuggest = ts.create(<AutoSuggest
            options={options}
            optionLabel={(option)=> option.value}
            label="simple"
            />)
          .toJSON();
          expect(autoSuggest).toMatchSnapshot();
    });
    
    test('Autosuggest component should render', () => {
         const testRenderer = ts.create(<AutoSuggest
            className={"auto-suggest"}
            options={options}
            optionLabel={(option)=> option.value}
            label="simple"
            />);
         const testInstance = testRenderer.root;
         expect(testInstance.findByType(AutoSuggest).props.className).toBe("auto-suggest");
         expect(testInstance.findByType(AutoSuggest).props.label).toBe("simple");
         expect(testInstance.findByType(AutoSuggest).props.options).toBe(options);
       });

});

