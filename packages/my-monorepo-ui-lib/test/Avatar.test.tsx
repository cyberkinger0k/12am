import React from 'react';
import * as ts from 'react-test-renderer';
import { Avatar } from '../src/components';

describe("Avatar components", () => {

    test("Snapshot testing", () => {
         const avatar = ts.create(<Avatar className = "avatar-1" id = "avatar"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"/>)
          .toJSON();
          expect(avatar).toMatchSnapshot();
    });
    
    test('Chart Component should render without error', () => {
         const testRenderer = ts.create(<Avatar className = "avatar-1" id = "avatar"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"/>);
         const testInstance = testRenderer.root; 
         expect(testInstance.findByType(Avatar).props.id).toBe('avatar');
         expect(testInstance.findByType(Avatar).props.className).toBe('avatar-1');
       });

});

