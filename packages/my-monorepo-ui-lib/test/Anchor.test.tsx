import React from 'react';
import * as ts from 'react-test-renderer';
import { A }  from '../src/components';

describe('Anchor Component', () => {
  it('Snapshot testing', () => {
    const tree = ts.create(<A content="Hello world" />)
      .toJSON();
    expect(tree).toMatchSnapshot();
    
  });

  it('Anchor should render correctly', () => {
    const testRenderer = ts.create(<A content="Hello world" className='wms-ui-a'/>);
    const testInstance = testRenderer.root;

    expect(testInstance.findByType(A).props.content).toBe('Hello world');
    expect(testInstance.findByType(A).props.className).toBe('wms-ui-a');
    
  });
});