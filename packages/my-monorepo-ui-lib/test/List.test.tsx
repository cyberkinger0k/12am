import React from 'react';
import * as ts from 'react-test-renderer';
import { ListItems, ListButton, Button, Li } from '../src/components';

describe("List", () => {

     test("Snapshot testing", () => {
          const list = ts.create( 
          <Li>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >
                <ListButton>
                <Button variant="contained" btnColor="primary">add</Button>
                <Button variant="contained" btnColor="secondary">del</Button>
                <Button variant="contained" btnColor="default">info</Button>
                </ListButton>    
            </ListItems> 
            </Li>)
           .toJSON();

           expect(list).toMatchSnapshot();
     });
     
     test('Input Component should render without error', () => {
          const testRenderer = ts.create(
          <Li>
            <ListItems
            primaryContent="hello"
            secondaryContent="its list component"
            avatarsrc="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B"
            >
                <ListButton>
                <Button variant="contained" btnColor="primary">add</Button>
                <Button variant="contained" btnColor="secondary">del</Button>
                <Button variant="contained" btnColor="default">info</Button>
                </ListButton>    
            </ListItems>
          </Li>);
          const testInstance = testRenderer.root;
      
          expect(testInstance.findByType(ListItems).props.primaryContent).toBe('hello');
          expect(testInstance.findByType(ListItems).props.secondaryContent).toBe('its list component');
          expect(testInstance.findByType(ListItems).props.avatarsrc).toBe("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-X-kOa-WjYYDEGX8cgLeaeXtVcTZ_5LjOBDhAnY0PBBa5D4B");
          expect(testInstance.findByType(ListButton).props.children.length).toBe(3);
        });

});
