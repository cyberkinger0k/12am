import { HttpService } from "./HttpService";
import { NavigationService } from "./NavigationService";

export interface ILoginProps {
    username: string;
    password: string;
}

export class AuthService {

    private _httpApi: HttpService;

    constructor() {
        this._httpApi = new HttpService("http://34.216.108.210/wms/v1.0/api");
        // this._httpApi = new HttpService("http://54.200.246.135/wms/test/v1.0/api");
        // this._httpApi = new HttpService("http://192.168.0.33:5055/v1.0/api");
    }

    public login(req: ILoginProps) {
        const headers = [
            { key: 'platform', value: 'browser' },
            { key: 'device', value: "TMS_"+Date.now() }
          ];
        return this._httpApi.postUser('/login', req, headers);
    }

    public logout(): void {
        this._httpApi.post("/users/logout","").then(() => {
            localStorage.removeItem("res");
            const loginUrl = "/login";
            new NavigationService().redirect(loginUrl);
        });
    }

    public forgot(email:string) {
        return this._httpApi.get(`/users/forgetPassword?email=${email}`); 
    }

    public reset(token:string,password:string) {
        return this._httpApi.put(`/users/setNewPassword?token=${token}`,password); 
    }

    public register(user: any) {
       
        return this._httpApi.postUser('/users/register', user);
    }

}
