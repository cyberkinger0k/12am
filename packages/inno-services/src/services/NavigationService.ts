
export class NavigationService {

    private history: any;

    constructor() {
        this.history = null;
    }

    public setHistory(history: History) {
        this.history = history;
    }

    public navTo(path: string) {
        this.history.push(path);
    }

    public redirect(linkInfo: string) {
        window.location.href = linkInfo;
    }
    public goBack() {
        this.history.goBack();
    }

}
