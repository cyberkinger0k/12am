import { ApiService } from "./ApiService";
import { AuthService } from "./AuthService";
import { LoggerService } from "./LoggerService";
import { NavigationService } from "./NavigationService";
import { StoreService } from "./StoreService";

class Builder {
    private instance: any;

    public build(creator: any) {
        if (!this.instance) {
            this.instance = creator();
        }
        return this.instance;
    }
}

export class AppServiceFactory {
    private _logger: any;
    private _nav: any;
    private _api: any;
    private _store: any;
    private _auth: any;

    constructor() {
        this._logger = new Builder();
        this._nav = new Builder();
        this._api = new Builder();
        this._store = new Builder();
        this._auth = new Builder();
    }

    get logger() {
        return this._logger.build(() => new LoggerService());
    }

    get nav() {
        return this._nav.build(() => new NavigationService());
    }

    get api() {
        return this._api.build(() => new ApiService());
    }

    get store() {
        return this._store.build(() => new StoreService());
    }

    get auth() {
        return this._auth.build(() => new AuthService());
    }
}

export const Asf = new AppServiceFactory();
