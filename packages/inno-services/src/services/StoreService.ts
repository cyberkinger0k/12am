import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

export class StoreService {

    private initStore: (reducer: any, initalState: any) => any;
    private store: any;

    constructor() {
        this.initStore = (reducer, initalState) => {
            this.store = createStore(reducer, initalState || {},
                applyMiddleware(thunk));
            return this.store;
        };
    }

    public getStore() {
        if (!this.store) {
            throw new Error("Store is not initialized yet!");
        }
        return this.store;
    }

    public dispatch(action: any) {
        return this.getStore().dispatch(action);
    }

    public getState() {
        return this.getStore().getState();
    }
}
