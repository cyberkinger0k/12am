import { Message } from "../common";

export class Utils {

    // Validates email //     
        static validateEmail = (email:string) => {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    // Validates name //
        static validateName = (name:string) => {
            var re = /^[a-z ,.'-]+$/i;
            return re.test(String(name));
        }
    // Not allow white spaces //
    static validatehiteSpaces = (name:string) => {
        var re = /^\s+$/;
        return re.test(String(name));
    }
    
    // Validates string cointain Alphanumeric //  
        static validateAlphaNumeric = (string:string) => {
            var re = /\w/;
            return re.test(String(string));
        }
    
    // Validate string only cointain alphabets //
        static validateAlphabets = (string:string) => {
            var re = /^[A-Za-z]+$/;
            return re.test(String(string));
        }
    
    // validate string is not more than 300 characters //    
        static validateCharacterSize300 = (string:any) => {
            let length = string && string.length; 
            if(length > 10 && length < 300){
                return true;
            }else{
                return false;
            }           
        }

    // validate string length less than 10 //    
        static validateMinLength10 = (string:any) => {
            let length = string && string.length; 
            if(length > 10){
                return true;
            }else{
                return false;
            }           
        }
    
    // Minimum eight characters, at least one letter, one number and one special character://
        static validatePassword = (password:string) => {
            var re = /"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$"/;
            return re.test(String(password));
        }
    
    // Validation for 2 digit number //
        static validationTwodigit = (num:number) => {
            if(num < 10  && num > 0 && num.toString().length < 2){
                return true;
            }else{
                return false;
            }
        } 
    
    // Validation for hall number //
    static validationHallNo = (num:number) => {
        if(num < 4  && num > 0 && num.toString().length < 2){
            return true;
        }else{
            return false;
        }
    }

    // Validation for 3 digit number //
        static validationThreedigit = (num:number) => {
            if(num < 100  && num > 0 && num.toString().length < 3){
                return true;
            }else{
                return false;
            }
        } 
    
    // Validation for asset id //
    static validationAssetId = (num:number) => {
        if(num < 1000  && num > 0 && num.toString().length < 4){
            return true;
        }else{
            return false;
        }
    } 
    
    // only check its not null or undefined //
    static requiredValidation = (test : any) => {
        var re = /\S/ ;
        if(test=="" || test==undefined ){
            return true;
        }
        else{
            return false;
        }
  }

  // word limit of 50 world//
  static wordLimit = (test : string) => {
    let re = /^.{5,100}$/ ;
    return re.test(String(test));
  }
  
  // deskno validation //
  static isValidDeskNo(deskNo:any) {

    let val = false;

    let splitString = deskNo.split(":");
    
    if (splitString.length != 2){
        return false;
    }
    val = this.validDeskNo(splitString[0], splitString[1]);

    return val;
}

  static validDeskNo(hall:string, desk:string) {
    var object = new Object("HALL");
    if (hall.length > 4 && hall.substring(0, 4).toLowerCase() == "hall"
            && parseInt(hall.substring(4)) > 0 && desk.length > 4
            && desk.substring(0, 4).toLowerCase() == "desk" && parseInt(desk.substring(4)) > 0){
        return true;
    } else{
        return false;
    }
}
// enter key check //
    static enterCheck(str:string) {
        for (let i = 0; i < str.length; i++) {
            if (str[i] === '\n' || str[i] === '\r') {
              return true;
            };
          };
    }
// asset no validation
        static isValidAssetId(assetId:any, department:any) {

        let departmentName;

        if (department == Message.Admin.ADMIN){
            departmentName = Message.Admin.ADMIN;
        } else if (department == Message.Admin.SNA){
            departmentName = Message.Admin.SNA;
        } else {
            departmentName = Message.Admin.HR;
        }

        let splitString = assetId.split("-");
        if (splitString.length != 4) {
            return false;
        }

        if (!(splitString[0]==="INNO")) {
            return false;
        }

        if (!(splitString[1]===departmentName)) {
            return false;
        }

        if (!(splitString[2].length > 1 && splitString[2].length < 7)) {
            return false;
        }

        if (parseInt(splitString[3]) <= 0) {
            return false;
        }

        return true;
        }

 // localstorage service
        static localstorage = (type:"get"|"set" ,name:string,  value?:any) => {
           if (type === "get") {
            let res = localStorage.getItem(name);
            if(res){
             return JSON.parse(res);
            }
           }
           else if (type === "set"){
               let value1 = JSON.stringify(value)
               if (typeof value1 === "string") {
                    localStorage.setItem(name,value1);
                    return true;
               }
        } 
    }
}