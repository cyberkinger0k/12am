import 'whatwg-fetch';
import { Utils } from '.';

export interface IApiRes {
    body: any;
}

export class HttpService {

    private apiHost: string;

    constructor(host: any) {
        this.apiHost = host;
    }
    public postUser(path: string, body: any, headers?: any) {
        return this.send('POST', path, body, headers);
    }
    public get(path: string, params?: Object) {
        if(params) {
            path = path +'?'+ this.getParams(params);
         }
        return this.sendWithToken("GET", path);
    }

    public post(path: string, body: any,params?: Object) {
        if(params) {
            path = path +'?'+ this.getParams(params);
         }
        return this.sendWithToken('POST', path, body);
    }

    public put(path: string, body: any, params?: Object) {
        if(params) {
           path = path +'?'+ this.getParams(params);
        }
        console.log(path);
        return this.sendWithToken("PUT", path, body);
    }
    public patch(path: string, body: any, params?: Object) {
        if(params) {
            path = path +'?'+ this.getParams(params);
         }
        return this.sendWithToken("PUT", path, body);
    }

    public delete(path: string) {
        return this.sendWithToken("DELETE", path);
    }

    public readBody(response: any) {
        return new Promise((resolve, reject) => {

            if (!response.ok) {
                response.json().then((body: any) => {
                    const apiResp: any = {};
                    apiResp.body = body;
                    resolve(apiResp);

                })

            } else {

                response.text().then((body: any) => {
                    const apiResp: any = {};
                    apiResp.body = JSON.parse(body);
                    resolve(apiResp);
                })
            }
        });
    }


    public parseJSON(response: any) {
        return new Promise((resolve, reject) => {
            resolve(response);
        })
    }


    private checkStatus(response: any) {
        if (response.body.status === "Error") {
            throw response.body;

        }
        return response.body;
    }

    send(httpMethod: string, path: string, body?: any, headers?: object[]) {
        const myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append("Access-Control-Allow-Origin", "*");
        myHeaders.append("Access-Control-Allow-Credentials", "true");
        if (headers && headers.length) {
            headers.forEach((element: any) => {
                myHeaders.append(element.key, element.value);
            });
        }

        const apiUrl = this.apiHost + path;
        return fetch(apiUrl, {
            method: httpMethod,
            credentials: 'include',
            headers: myHeaders,
            body: body ? JSON.stringify(body) : undefined
        })
            .then(this.readBody)
            .then(this.checkStatus)
            .then(this.parseJSON, (error: any) => {
                throw error;
            })
    
    }

    sendWithToken = (httpMethod: string, path: string, body?: any) => {
        
        const res = Utils.localstorage("get","res");
        let token: any;
        if (res && res !== null && res !=undefined) {
            
            token = {
                key: "authToken", value: res.authToken
            }
        }
        
        return this.send(httpMethod, path, body, token !=undefined ? [token]: []);
    }

    getParams(params:any) {
        const body: any = new URLSearchParams();
        if (params != null) {
          Object.keys(params).forEach(key => {
            body.set(key, params[key]);
          });
        }
        return body.toString();
    }
}
