import { HttpService } from "./HttpService";

export class ApiService {

    private _httpApi: HttpService;

    constructor() {
        this._httpApi = new HttpService("http://34.216.108.210/wms/v1.0/api");
        // this._httpApi = new HttpService("http://54.200.246.135/wms/test/v1.0/api");
        // this._httpApi = new HttpService("http://192.168.0.33:5055/v1.0/api");
    }

    public getUsers() {
        return this._httpApi.get("/users");
    }

    public update(path:string, body?:any, params?: Object) {
        return this._httpApi.put(path,body, params);    
    }

    public getProfile(path:string, params?: Object) {
        return this._httpApi.get(path, params);
    }

    public post(path:string,body:any, params?: Object) {
        return this._httpApi.post(path,body,params);
    }
    public patch(path:string , body:any, params?: Object){
        return this._httpApi.patch(path,body, params);
    }

    public get(path:string, params?: Object) {
        return this._httpApi.get(path, params);
    }
    public delete(path:string) {
        return this._httpApi.delete(path);    
    }

}
