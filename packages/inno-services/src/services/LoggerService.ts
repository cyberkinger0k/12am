export class LoggerService {

    public log(args: string) {
        console.log(args);
    }

    public warn(args: string) {
        console.warn(args);
    }

    public error(args: string) {
        console.error(args);
    }

    public info(args: string) {
        console.info(args);
    }

    public debug(args: string) {
        console.debug(args);
    }
}
