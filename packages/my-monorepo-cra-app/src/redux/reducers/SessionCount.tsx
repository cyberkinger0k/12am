const SessionCount = (state = false, action:any) => {
    switch (action.type) {
        case 'COUNT':
          return (action.count);
        default:
          return state;
      }
};
export default SessionCount;