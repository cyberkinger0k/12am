import React from 'react';
import * as ts from 'react-test-renderer';
import { Login }  from './Login';

test("Login should rendered correctly", () =>{

    const testRenderer = ts.create(<Login/>);
    const testInstance = testRenderer.root;
 
    expect(testInstance.findAllByType('div').length).toBeGreaterThan(1);
});
