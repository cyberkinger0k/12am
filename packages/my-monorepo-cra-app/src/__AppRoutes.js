import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Inbox, Wms_Inbox } from '../src/pages';
import { Feedbacks } from '../src/pages';
import { FilledFeedbackForm } from '../src/pages';
import { CreateSession, Dashboard } from './pages';
//import SubmittedFeedbacks from '../src/pages/submittedFeedbacks/SubmittedFeedbacks';
import { FeedbackForm , FeedbackStatus} from './pages';


//import SubmittedFeedbacks from '../src/pages/submittedFeedbacks/SubmittedFeedbacks';

const renderInboxPage = (context) => {
    return <Inbox />
}
const renderFeedbackPage = (context) => {
    return <Feedbacks/>
}
const renderProvideFeedbackPage = (context) => {
    return <FeedbackForm/>
}
const renderFeedbackFormPage = (context) => {
    return <FilledFeedbackForm/>
}
const renderCreateSession = (context) => {
    return <CreateSession />
}

//workflow-system-pages

const renderWorkflowInboxPage = (context) => {
    return <Wms_Inbox />
}
const renderFeedbackStatus = (context) => {
    return <FeedbackStatus/>
}

export const AppRoutes = (props) => {
    return (
        <div className='page-content'>
            <Switch>
                <Route exact path='/' component={(context) => renderInboxPage(context)} />
                <Route exact path='/Feedbacks' component={(context) => renderFeedbackPage(context)} />
                <Route exact path='/provide-feedback-form' component={(context) => renderProvideFeedbackPage(context)} />
                <Route exact path='/filled-feedback-form' component={(context) => renderFeedbackFormPage(context)} />
                <Route exact path='/CreateSession' component={(context) => renderCreateSession(context)} />
                <Route exact path='/status' component={(context) => renderFeedbackStatus(context)} />
                <Route exact path='/ticket' component={(context) => renderWorkflowInboxPage(context)} />
            </Switch>
        </div>
    );
}
