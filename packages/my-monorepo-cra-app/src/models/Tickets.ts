export type Ticket = 
{
    title?: string,
    department?:'HR' | 'ADMIN' | 'SNA',
    resolution?: string,
    comment?: string,
    priority?: 'High'|'Medium'|'Low',
    deskNo?: string,
    hall_no?:string,
    desk_no?:string,
    assetId?: string,
    description?: string,
    created?: string,
    assigned?: string,
    updated?: string,
    status?: 'NEW' | 'INPROCESS' | 'CLOSED',
    assignee?:string,
    assetid?: string,
    asset_no?: number,
    asset?: string,

}